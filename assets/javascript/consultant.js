$ = jQuery;
console.log('test');
$('input#consultant_search_field').keyup(function() {
    
   var search = $(this).val();
   var data = {
	    action : "my_ajax_filter_search",
	    search : search
	}

	$.ajax({
        url : ajax_url,
        data : data,
        success : function(response) {

          // console.log( response );
          $("#consultant_results").empty();
          var html = '<ul>';

          $.each(response, function() {
            console.log( this['data'] );
            html += '<li class="consultant_item">' + '<img src="' + this['data'].avatar + '" />' + '<h2>' + this['data'].display_name + '</h2>' + '<label for="consultant_list">Select ' + this['data'].display_name + '</label><input name="consultant_list" type="radio" value="' + this['data'].display_name + '_' + this['data'].ID + '_' + this['data'].user_email + '" id="consultant_id_' + this['data'].ID + '"/></li>';
          });

          html += '</ul>';
          $("#consultant_results").html(html);
        } 
    });

});