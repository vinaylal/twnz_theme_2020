function pp_member_search_radial() {

	var input = document.getElementById('pp_member_search_center');
	var options = {types: ['geocode']};
	var autocomplete = new google.maps.places.Autocomplete(input, options);
	autocomplete.setFields(['geometry']);

	google.maps.event.addListener(autocomplete, 'place_changed', function() {
		var place = autocomplete.getPlace();
		if ( place.geometry ) {
			console.log(place);
			var lat = place.geometry.location.lat();
			var lng = place.geometry.location.lng();
			var latlng = lat + ',' + lng;
			document.getElementById('pp_member_search_center_coords').value = latlng;
		}
	});
}
jQuery(document).ready (pp_member_search_radial);