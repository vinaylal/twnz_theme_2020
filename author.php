<?php

/*
 * Template Name: Author Page
 * description: >-
 */

?>
<?php get_header(); ?>
<?php $adjust_middle_column = false; ?>
<?php $CurrentPage = home_url( add_query_arg( array(), $wp->request ) ); ?>

<div id="content">
    <section class="row" data-test="vinay">
        
        <div id="leftColumn" class="col">
            <div class="wrapper">
                <div id="leftNavigation">
                    <?php get_template_part( 'template-parts/categories-menu' ); ?>
                </div>
            </div>
        </div>
        
        <div id="middleColumn" class="col <?php if($adjust_middle_column){ echo 'adjust_middle_column';} ?>">
            <div class="wrapper">
                
                <?php

                    if ( is_archive() && ! have_posts() ) {
                        $archive_title = __( 'Nothing Found', 'twentytwenty' );
                    } elseif ( ! is_home() ) {
                        $archive_title    = get_the_archive_title();
                        $archive_subtitle = get_the_archive_description();
                    }

                    if ( $archive_title || $archive_subtitle ) {
                        ?>

                        <header class="archive-header has-text-align-center header-footer-group">

                            <div class="archive-header-inner section-inner medium">

                                <?php if ( $archive_title ) { ?>
                                    <h1 class="archive-title"><?php echo wp_kses_post( $archive_title ); ?></h1>
                                <?php } ?>

                                <?php if ( $archive_subtitle ) { ?>
                                    <div class="archive-subtitle section-inner thin max-percentage intro-text"><?php echo wp_kses_post( wpautop( $archive_subtitle ) ); ?></div>
                                <?php } ?>

                            </div><!-- .archive-header-inner -->

                        </header><!-- .archive-header -->

                        <?php
                    }

                    if ( is_home() ){
                        echo '<h1>Blog</h1>';
                    }
                    
                    if ( have_posts() ) {
                        
                        while ( have_posts() ) {
                            
                            the_post();
                            echo '<a href="' . get_permalink() . '">';
                            the_title( '<h1 class="entry-title">', '</h1>' );
                            echo '</a>';
                            echo '<a href="' . get_permalink() . '">';
                            the_post_thumbnail();
                            echo '</a>';
                            the_excerpt();
                            echo '<a href="' . get_permalink() . '">Read More...</a>';
                        }

                        get_template_part( 'template-parts/pagination' );
                    }
                ?>                                
                
            </div>
        </div>
                            
    </section>
</div>
<?php get_footer(); ?>