<?php
/**
 * Members Directory Map Filters Template
 *
 * You can copy this file to your-theme/buddypress/members/
 * and then edit the layout.
 */

if ( !defined( 'ABSPATH' ) ) exit;

do_action( 'bp_before_members_page_map_filters' );

?>

	<div class="members-dir-map-search-div">
	<div>


<?php do_action( 'bp_after_members_page_map_filters' ); ?>