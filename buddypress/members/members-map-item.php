<?php

/**
 * Members Directory Map Item Template - For Item Popup on Map
 *
 * You can copy this file to your-theme/buddypress/members/
 * and then edit the layout.
 *
 * For an example of adding another field to this template,
 * please see: bp-maps-for-members/readme-add-popup-field.txt
 *
 */

?>

<div class="members-map-pin-popup">

	<?php if ( isset( $avatar ) ) echo $avatar; ?>

	<?php if ( isset( $title ) ) print_r('<h3>' . $title . '</h3>'); ?>

	<?php if ( isset( $telephone ) ) print_r('<p>' . $telephone . '</p>'); ?>

</div>