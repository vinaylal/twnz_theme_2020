<?php
global $wp;
$detect = new Mobile_Detect;
?>
    <footer>
        <?php

            $CurrentPage = home_url( add_query_arg( array(), $wp->request ) );

            if ( 
                // $CurrentPage == 'https://tupperware.co.nz/contact-us' ||
                $CurrentPage == 'https://tupperware.co.nz/cart' ||
                $CurrentPage == 'https://tupperware.co.nz/checkout' ||
                // $CurrentPage == 'https://dev.twnz.co.nz/contact-us' ||
                $CurrentPage == 'https://dev.twnz.co.nz/cart' ||
                $CurrentPage == 'https://dev.twnz.co.nz/checkout' ||
                // $CurrentPage == 'https://tupperware.twnz.co.nz/contact-us' ||
                $CurrentPage == 'https://tupperware.twnz.co.nz/cart' ||
                $CurrentPage == 'https://tupperware.twnz.co.nz/checkout'
            ){
                ?>
                
                <script>
                    var question = jQuery('.nAccordian .question');
                    var answer = jQuery('.nAccordian .answer');

                    jQuery(question).click(function(){
                        jQuery(answer).slideUp();
                        jQuery(this).next().slideToggle('fast');
                    });

                    // Number type input fields
                    jQuery('<div class="quantity-nav"><div class="quantity-button quantity-up">+</div><div class="quantity-button quantity-down">-</div></div>').insertAfter('.quantity input');
                    jQuery('.quantity').each(function() {
                      var spinner = jQuery(this),
                        input = spinner.find('input[type="number"]'),
                        btnUp = spinner.find('.quantity-up'),
                        btnDown = spinner.find('.quantity-down'),
                        min = input.attr('min'),
                        max = input.attr({
                           "max" : 1000
                        });

                      btnUp.click(function() {

                        var oldValue = parseFloat(input.val());

                        if (oldValue >= max) {
                          var newVal = oldValue;
                        } else {
                          var newVal = oldValue + 1;

                        }
                        spinner.find("input").val(newVal);
                        spinner.find("input").trigger("change");
                      });

                      btnDown.click(function() {
                        var oldValue = parseFloat(input.val());
                        if (oldValue <= min) {
                          var newVal = oldValue;
                        } else {
                          var newVal = oldValue - 1;
                        }
                        spinner.find("input").val(newVal);
                        spinner.find("input").trigger("change");
                      });

                    });
                </script>

                <?php if( is_page('home') || is_page('cart') || is_page('checkout') ){ ?>

                    <script type="text/javascript">
                        // jQuery(function(){
                        //   SyntaxHighlighter.all();
                        // });
                        jQuery(window).load(function(){
                          jQuery('.flexslider').flexslider({
                            controlNav: false,  
                            animation: "slide",
                            prevText: "",
                            nextText: "",  
                            start: function(slider){
                              jQuery('body').removeClass('loading');
                            }
                          });
                        });
                      </script>
                  <?php }

            }else{
                // show the footer
                ?>
                    <section class="row">
                        <div id="bottomSubNavigation">
                            <nav>
                                <?php clean_custom_menu("twnz-custom-subnav-footer"); ?>
                            </nav>
                        </div>
                    </section>
                    <section class="row">
                        <div class="content">
                            <div class="tupperware-logo">
                                <div class="logo icon--tupperware-logo"></div>
                            </div>
                            <div class="teasers">
                                <ul>
                                    <li>
                                        <?php $link = get_field('catalogue_link' , 'option'); ?>
                                        <a href="<?php print_r($link); ?>" class="icon--catalog" target="_self">
                                            <span>Catalogue</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php the_field('newsletter_link' , 'option'); ?>" class="icon--newsletter" target="_self">
                                            <span>Newsletter</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="social-media">
                                <ul>
                                    <li>
                                        <a href="<?php the_field('facebook_link' , 'option'); ?>" class="facebook icon--facebook-po" target="_blank">
                                            <span>Follow on Facebook</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php the_field('youtube_link' , 'option'); ?>" class="youtube icon--youtube-po" target="_blank">
                                            <span>Follow on YouTube</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php the_field('instagram_link' , 'option'); ?>" class="instagram icon--instagram-po" target="_blank">
                                            <span>Follow on Instagram</span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="<?php the_field('pintrest_link', 'option'); ?>" class="pinterest icon--pinterest-po" target="_blank">
                                            <span>Follow on Pinterest</span>
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </section>
                    <section class="row last-section">
                        <div class="content">
                            <div class="bottom-navigation">
                                <?php clean_custom_menu("twnz-custom-menu-footer"); ?>
                            </div>
                        </div>
                    </section>
                    <section class="row"></section>
                    <script type="text/javascript">
                        if (typeof NProgress != 'undefined') NProgress.start();
                    </script>
                    <?php wp_footer(); ?>
                    <script>

                        var maxHeight = 0;

                        jQuery(".equalise").each(function(){
                           
                           if (jQuery(this).height() > maxHeight) { 
                                
                                maxHeight = jQuery(this).height(); 
                            }
                        });

                        jQuery(".equalise").height(maxHeight);

                    </script>
                    <script>
                        var question = jQuery('.nAccordian .question');
                        var answer = jQuery('.nAccordian .answer');

                        jQuery(question).click(function(){
                            jQuery(answer).slideUp();
                            jQuery(this).next().slideToggle('fast');
                        });
                        // tab functionality on product pages
                        // From http://learn.shayhowe.com/advanced-html-css/jquery
                        jQuery('.tabs a').on('click', function (event) {
                            event.preventDefault();
                            
                            jQuery('.tab-active').removeClass('tab-active');
                            jQuery(this).parent().addClass('tab-active');
                            jQuery('.tabs-stage div').hide();
                            jQuery(jQuery(this).attr('href')).show();
                        });

                        jQuery('.tabs-nav a:first').trigger('click'); // Default

                        // Number type input fields
                            jQuery('<div class="quantity-nav"><div class="quantity-button quantity-up">+</div><div class="quantity-button quantity-down">-</div></div>').insertAfter('.quantity input');
                            jQuery('.quantity').each(function() {
                              var spinner = jQuery(this),
                                input = spinner.find('input[type="number"]'),
                                btnUp = spinner.find('.quantity-up'),
                                btnDown = spinner.find('.quantity-down'),
                                min = input.attr('min'),
                                max = input.attr({
                                   "max" : 1000
                                });

                              btnUp.click(function() {

                                var oldValue = parseFloat(input.val());

                                if (oldValue >= max) {
                                  var newVal = oldValue;
                                } else {
                                  var newVal = oldValue + 1;

                                }
                                spinner.find("input").val(newVal);
                                spinner.find("input").trigger("change");
                              });

                              btnDown.click(function() {
                                var oldValue = parseFloat(input.val());
                                if (oldValue <= min) {
                                  var newVal = oldValue;
                                } else {
                                  var newVal = oldValue - 1;
                                }
                                spinner.find("input").val(newVal);
                                spinner.find("input").trigger("change");
                              });

                            });
                    </script>

                    <?php if( is_page('home') || is_page('cart') || is_page('checkout') ){ ?>

                        <script type="text/javascript">
                            jQuery(window).load(function(){
                              jQuery('.flexslider').flexslider({
                                controlNav: false,
                                animation: "slide",
                                prevText: "",
                                nextText: "",  
                                start: function(slider){
                                  jQuery('body').removeClass('loading');
                                }
                              });
                            });
                          </script>
                      <?php } ?>
                <?php
            }

        ?>
    </footer>

    <?php

        $CurrentPage = home_url( add_query_arg( array(), $wp->request ) );

        if ( 
            $CurrentPage == 'https://tupperware.co.nz/connect' ||
            $CurrentPage == 'https://tupperware.co.nz/cart' ||
            $CurrentPage == 'https://tupperware.co.nz/checkout' ||
            $CurrentPage == 'https://dev.twnz.co.nz/connect' ||
            $CurrentPage == 'https://dev.twnz.co.nz/cart' ||
            $CurrentPage == 'https://dev.twnz.co.nz/checkout' ||
            $CurrentPage == 'https://tupperware.twnz.co.nz/connect' ||
            $CurrentPage == 'https://tupperware.twnz.co.nz/cart' ||
            $CurrentPage == 'https://tupperware.twnz.co.nz/checkout'
        ){
            // do not show button
            // break;
        }else{ 
            get_template_part( 'template-parts/cta' );
        }

    ?>

</div>
</div>
<!-- cover -->
</div>
</body>
</html>