<?php

// Including our mobile detection software
    require_once "classes/Mobile-Detect/Mobile_Detect.php";
// Add a custom nav to WP
    function wpb_custom_new_menu() {

        $locations = array(
            'twnz-custom-menu'  => __( 'TWNZ Custom Menu', 'twnz.co.nz' ),
            'twnz-custom-menu-footer' => __( 'TWNZ Custom Menu Footer', 'twnz.co.nz' ),
            'twnz-custom-subnav-footer'   => __( 'TWNZ Custom Subnav Footer', 'twnz.co.nz' )
        );

        register_nav_menus( $locations );
    }

    add_action( 'init', 'wpb_custom_new_menu' );

// Register and Enqueue Scripts.
 
    function custom_register_scripts() {

        $theme_version = wp_get_theme()->get( 'Version' );

        if ( ( ! is_admin() ) && is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
            wp_enqueue_script( 'comment-reply' );
        }

        wp_enqueue_script( 'twnz-js', get_template_directory_uri() . '/assets/javascript/script.js', array(), $theme_version, false );
        wp_enqueue_style( 'twnz-css', get_stylesheet_directory_uri() . '/style.css', '', $theme_version, false );
        
        wp_script_add_data( 'twnz-js', 'async', true );

        if(is_page('home') || is_page('cart') || is_page('checkout')){

            wp_enqueue_script( 'flexslider-js', get_template_directory_uri() . '/assets/javascript/flexslider.js', array('jquery'), $theme_version, false );

            wp_enqueue_style( 'flexslider-css', get_stylesheet_directory_uri() . '/assets/css/flexslider.css', '', $theme_version, false );
        }
    }

    add_action( 'wp_enqueue_scripts', 'custom_register_scripts' );

//on the fly resizing of images
    //require_once('classes/aq_resizer.php');

// Let Wocommerce know we are creating a custom theme.
    function custom_theme_setup() {
            
            add_theme_support( 'title-tag' );
            add_theme_support( 'woocommerce' );
            // Add default posts and comments RSS feed links to head.
            add_theme_support( 'automatic-feed-links' );
            /*
             * Enable support for Post Thumbnails on posts and pages.
             *
             * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
             */
            add_theme_support( 'post-thumbnails' );

            // Set post thumbnail size.
            set_post_thumbnail_size( 1200, 9999 );

             /*
             * Switch default core markup for search form, comment form, and comments
             * to output valid HTML5.
             */
            add_theme_support(
                'html5',
                array(
                    'search-form',
                    'comment-form',
                    'comment-list',
                    'gallery',
                    'caption',
                    'script',
                    'style',
                )
            );

            // Add support for responsive embeds.
            add_theme_support( 'responsive-embeds' );

            // Add theme support for selective refresh for widgets.
            add_theme_support( 'customize-selective-refresh-widgets' );
    }

    add_action( 'after_setup_theme', 'custom_theme_setup');

// Add some CSS to Woocommerce emails
 
    function custom_add_css_to_emails( $css, $email ) { 
       $css .= '
          h2 { text-align: center!important; }
       ';
       return $css;
    }

    add_filter( 'woocommerce_email_styles', 'custom_add_css_to_emails', 9999, 2 );

// Add a function which will loop through child menu items easily to replicate existing menu structure of the site
    function clean_custom_menu( $theme_location ) {
        
        //if ( ($theme_location) && ($locations = get_nav_menu_locations()) && isset($locations[$theme_location]) ) {
            // $menu = get_term( $theme_location, 'nav_menu' );
            $menu_items = wp_get_nav_menu_items($theme_location);
     
            $menu_list = '<ul>' ."\n";
     
            $count = 0;
            $submenu = false;
             
            foreach( $menu_items as $menu_item ) {
                 
                $link = $menu_item->url;
                $title = $menu_item->title;
                 
                if ( !$menu_item->menu_item_parent ) {
                    
                    $parent_id = $menu_item->ID;
                    
                    if( $theme_location == 'twnz-custom-subnav-footer'){

                        $menu_list .= '<li>' ."\n";

                    }else{

                        $menu_list .= '<li class="oneCol">' ."\n";
                    }
                    if( $title == 'Shop' ){

                        $menu_list .= '<a href="'.$link.'" class="title"><span><i style="color:#ff7000 !important;" class="fas fa-shopping-basket"></i> '.$title.'</span></a>' ."\n";
                    }else{

                        $menu_list .= '<a href="'.$link.'" class="title"><span>'.$title.'</span></a>' ."\n";
                    }
                }
                
                // if the top level item has child menu items then lets display them
                if ( $parent_id == $menu_item->menu_item_parent && $theme_location != 'twnz-custom-subnav-footer') {
                    
                    if ( !$submenu ) {
                        $submenu = true;
                        $menu_list .= '<div class="over"><div class="wrapper"><ul>' ."\n";
                    }
     
                    $menu_list .= '<li>' ."\n";
                    $menu_list .= '<a href="'.$link.'" class="title"><span>'.$title.'</span></a>' ."\n";
                    $menu_list .= '</li>' ."\n";
                         
                    if( isset( $menu_items[ $count + 1 ] ) ){

                        if ( $menu_items[ $count + 1 ]->menu_item_parent != $parent_id && $submenu ){
                            $menu_list .= '</ul></div></div>' ."\n";
                            $submenu = false;
                        }
                    }
     
                }
                
                // If no children then lets just end the element
                if( isset( $menu_items[ $count + 1 ] ) ){
                    if ( $menu_items[ $count + 1 ]->menu_item_parent != $parent_id ) { 
                        $menu_list .= '</li>' ."\n";      
                        $submenu = false;
                    }
                }
     
                $count++;
            }

            $year = date("Y");

            if( $theme_location == 'twnz-custom-menu-footer'){
                $menu_list .= '<li>&copy; ' . $year . ' Tupperware. All rights reserved.</li>';
            }

            if( $theme_location == 'twnz-custom-menu'){

                if(isset($_GET['bankCheck'])){
                  
                    $menu_list .= '<li class="oneCol">' ."\n";

                }else{
                  
                    $menu_list .= '<li style="display:none" class="oneCol">' ."\n";
                }

                $menu_list .= '<a href="/cart" class="title"><span><i style="color:#ff7000 !important;" class="fas fa-shopping-cart"></i> Cart</span></a>' ."\n";
                $menu_list .= '</li>' ."\n";
            }

            $menu_list .= '</ul>' ."\n";
     
        // } else {

        //     $menu_list = '<!-- no menu defined in location "' . $theme_location . '" -->';
            
        // }

        echo $menu_list;
    }

// Woocommerce hooks
    
    add_action( 'woocommerce_before_cart_table', 'custom_before_cart_table' );
 
    function custom_before_cart_table(){
        echo '<h1>You selected the following item(s) for checkout</h1>';
    }
// End of Woocommerce hooks

// This code needs to go in the active theme (if tupperware.co.nz is not the active one). Add page tempaltes from another theme to the page template drop down.

    function twnz_add_page_template ($templates) {

        $templates['custom-connect-page.php'] = 'twnz.co.nz - Connect Page Template';
        $templates['custom-faq-page.php'] = 'twnz.co.nz - FAQ Template';
        $templates['custom-find-brands.php'] = 'twnz.co.nz - Find WordWide Template';
        $templates['custom-home-page.php'] = 'twnz.co.nz - Home Page Template';
        $templates['custom-product-category-page.php'] = 'twnz.co.nz - Product Category Template';
        $templates['custom-product-template.php'] = 'twnz.co.nz - Product Template';
        $templates['custom-slider-page.php'] = 'twnz.co.nz - Slider Template';
        $templates['page.php'] = 'twnz.co.nz - Generic Page Template';
        $templates['custom-slider-page-alternative.php'] = 'twnz.co.nz - Custom Slider Alt Page Template';
        $templates['custom-contact-page.php'] = 'twnz.co.nz - Custom Contact Page Template';
        $templates['custom-find-brands.php'] = 'twnz.co.nz - Tupperware Worldwide Page Template';

        return $templates;
    }

    add_filter ('theme_page_templates', 'twnz_add_page_template');

    function twnz_redirect_page_template ($template) {
        
        $post = get_post();
        $page_template = get_post_meta( $post->ID, '_wp_page_template', true );

        if ('custom-connect-page.php' == basename ($page_template)){

            $template = WP_CONTENT_DIR . '/themes/twnz_theme_2020/templates/custom-connect-page.php';

        }elseif('custom-faq-page.php' == basename ($page_template)){

            $template = WP_CONTENT_DIR . '/themes/twnz_theme_2020/templates/custom-faq-page.php';
        
        }elseif('custom-find-brands.php' == basename ($page_template)){

            $template = WP_CONTENT_DIR . '/themes/twnz_theme_2020/templates/custom-find-brands.php';
        
        }elseif('custom-home-page.php' == basename ($page_template)){

            $template = WP_CONTENT_DIR . '/themes/twnz_theme_2020/templates/custom-home-page.php';
        
        }elseif('custom-product-category-page.php' == basename ($page_template)){

            $template = WP_CONTENT_DIR . '/themes/twnz_theme_2020/templates/custom-product-category-page.php';
        
        }elseif('custom-product-template.php' == basename ($page_template)){

            $template = WP_CONTENT_DIR . '/themes/twnz_theme_2020/custom-product-template.php';
        
        }elseif('custom-slider-page.php' == basename ($page_template)){

            $template = WP_CONTENT_DIR . '/themes/twnz_theme_2020/templates/custom-slider-page.php';
        
        }elseif('page.php' == basename ($page_template)){

            $template = WP_CONTENT_DIR . '/themes/twnz_theme_2020/page.php';

        }elseif('custom-slider-page-alternative.php' == basename ($page_template)){

            $template = WP_CONTENT_DIR . '/themes/twnz_theme_2020/templates/custom-slider-page-alternative.php';

        }elseif('custom-contact-page.php' == basename ($page_template)){

            $template = WP_CONTENT_DIR . '/themes/twnz_theme_2020/templates/custom-contact-page.php';
        
        }elseif('custom-find-brands.php' == basename ($page_template)){

            $template = WP_CONTENT_DIR . '/themes/twnz_theme_2020/templates/custom-find-brands.php';
        }

        return $template;
    }

    add_filter ('page_template', 'twnz_redirect_page_template');

// Add options page to ACF
    if( function_exists('acf_add_options_page') ) {
        acf_add_options_page();
    }

// Remove some things from checkout page

    remove_action( 'woocommerce_checkout_order_review', 'woocommerce_order_review', 10 );
             
    // add the action 
    add_action( 'woocommerce_checkout_after_customer_details', 'action_woocommerce_checkout_after_customer_details', 10, 1 );

    // This code is here for posterity or if the theme is to be used by itself. WHen using multiple themes, as we are now, this code will only work if it is put into the 'active' theme functions.php. By active I mean the one set as active in the 'Themes' area of the CMS.

    function my_ajax_filter_search_scripts() {
        wp_enqueue_script( 'my_ajax_filter_search', get_template_directory_uri() . '/assets/javascript/consultant.js', array(), '1.0', true );
        wp_localize_script( 'my_ajax_filter_search', 'ajax_url', admin_url('admin-ajax.php') );
    }

    function my_ajax_filter_search_callback() {
 
        header("Content-Type: application/json");

        // Setup arguments.
        $args = array(
            'role' => 'group_leader',
            'fields'     => 'all_with_meta',
            'meta_query' => array(
                'relation' => 'OR',
                array(
                    'key'     => 'address',
                    'value'   => sanitize_text_field($_REQUEST['search']),
                    'compare' => 'LIKE'
                )
            )

        );
         
        // Custom query.
        $my_user_query = new WP_User_Query( $args );
         
        // Get query results.
        $users = $my_user_query->get_results();

        // add usermeta data to the returned result
        foreach( $users as $user ){
            $id = $user->data->ID;
            $user->data->avatar = get_avatar_url($id);
        }

        print_r( json_encode( $users ) );

        wp_die();
    }

    add_action('wp_ajax_my_ajax_filter_search', 'my_ajax_filter_search_callback');
    add_action('wp_ajax_nopriv_my_ajax_filter_search', 'my_ajax_filter_search_callback');

// Adding consultant info to the order and various other things

    add_action( 'woocommerce_after_checkout_billing_form', 'my_custom_checkout_field' );

    function my_custom_checkout_field( $checkout ) {

        my_ajax_filter_search_scripts();
        echo '<div id="my_custom_checkout_field"><h2>' . __('Choose a consultant to send your order to:') . '</h2>';

        woocommerce_form_field( 'consultant_search_field', array(
            'type'          => 'text',
            'class'         => array('my-field-class form-row-wide'),
            'label'         => __('Your order will be processed by one of our local representatives. Please type your location to find our nearest consultant.'),
            'placeholder'   => __('Your nearest town or suburb'),
            ), $checkout->get_value( 'consultant_search_field' ));

        echo '<div id="consultant_results">';
        echo '</div>';
        echo '</div>';

    }

    // Add this back in when you have trouble shot consultant list problem.
    // add_action('woocommerce_checkout_process', 'my_custom_checkout_field_process');

    function my_custom_checkout_field_process() {
 
        // Check if set, if its not set add an error.
        // if ( ! $_POST['consultant_search_field'] )
        //     wc_add_notice( __( 'Please search for a consultant to complete your order.' ), 'error' );
        if ( ! $_POST['consultant_list'] )
            wc_add_notice( __( 'Please search for and select a consultant to complete your order.' ), 'error' );
    }

    add_action( 'woocommerce_checkout_update_order_meta', 'my_custom_checkout_field_update_order_meta' );

    function my_custom_checkout_field_update_order_meta( $order_id ) {
        
        if ( ! empty( $_POST['consultant_search_field'] ) ) {

            update_post_meta( $order_id, 'ConsultantSearch', sanitize_text_field( $_POST['consultant_search_field'] ) );
        }

        if ( ! empty( $_POST['consultant_list'] ) ) {

            update_post_meta( $order_id, 'Consultant', sanitize_text_field( $_POST['consultant_list'] ) );
        }
    }

    add_action( 'woocommerce_admin_order_data_after_billing_address', 'my_custom_checkout_field_display_admin_order_meta', 10, 1 );

    function my_custom_checkout_field_display_admin_order_meta($order){
        echo '<p><strong>'.__('Consultant').':</strong> ' . get_post_meta( $order->id, 'Consultant', true ) . '</p>';
        echo '<p><strong>'.__('ConsultantSearch').':</strong> ' . get_post_meta( $order->id, 'ConsultantSearch', true ) . '</p>';
    }

// Add consultant to the 'new order' email
    add_filter( 'woocommerce_email_headers', 'bbloomer_order_completed_email_add_cc_bcc', 9999, 3 );
 
    function bbloomer_order_completed_email_add_cc_bcc( $headers, $email_id, $order ) {

        $consultant_exploded = explode( '_' , get_post_meta( $order_id, 'Consultant', true ) );
        $consultantEmail = $consultant_exploded[2];

        if ( 'new_order' == $email_id ) {
            $headers .= "Cc: Consultant <" .  $consultantEmail . ">" . "\r\n";
            $headers .= "Bcc: Vinay <hello@vinaylal.co>" . "\r\n";
        }
        
        return $headers;
    }

// Add consultatnt to the 'order-received' page 
    add_action( 'woocommerce_thankyou_order_received_text', 'bbloomer_add_content_thankyou' );
 
    function bbloomer_add_content_thankyou( $order_id ) {

        echo '<h2 class="woocommerce-order-details__title">What Next?</h2>';
        echo '<p>The consultant you selected during the checkout process has been notified by email of your order and will follow up soon with details of online payment and delivery.</p>';
       
    }
     
// Allow HTML in author bio section 
    remove_filter('pre_user_description', 'wp_filter_kses');

// Move woocommerce wishlist button
    function prefix_wishlist_template_location( $template_hook, $product_id ) {
     // Return your hook here
     return 'woocommerce_before_add_to_cart_button';
    }
    add_filter( 'woocommerce_wishlists_template_location', 'prefix_wishlist_template_location', 10, 2 );

// Send an API call to tupperware dropshipper upon order completion
    
    add_action( 'woocommerce_thankyou', 'action_woocommerce_thankyou', 10, 1 ); 

    function action_woocommerce_thankyou( $order_get_id ) { 
        
        $order = wc_get_order( $order_get_id );
        $id = $order->get_id();
        $date = new DateTime( $order->get_date_created() );
        $formattedDate = str_replace("/", "", $date->format('m/d/Y'));
        
        include_once( get_stylesheet_directory() .'/includes/JDE_XML.php');

        $url = "https://116.51.13.194/Local_Service_NZ/Proxy_Service/SalesOrderEDIInbound/PS_Local_NZL_SalesOrderEDIInbound_Req";

        $ch = curl_init();
        if (!$ch) {
            die("Couldn't initialize a cURL handle");
        }

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: text/xml'));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xmldata);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        $result = curl_exec($ch);
        echo $result;
        
        print_r($xmldata);

        // $subt = (float)$item->get_subtotal();
        // $qty = (float)$item->get_quantity();
        // $SZAEXP = ($subt) * ($qty);

        // if( curl_error($ch) ){

        //     $msg = curl_error($ch);
        //     $to = 'onethruten@gmail.com';
        //     $subject = 'Error in JDE API order creation process';
        //     $body = $msg;
        //     $headers = array('Content-Type: text/html; charset=UTF-8');
        //     wp_mail( $to, $subject, $body, $headers );

        // }else{
            
        //     $msg = curl_error($ch);
        //     $to = 'onethruten@gmail.com';
        //     $subject = 'Success in JDE API order creation process';
        //     $body = $result;
        //     $headers = array('Content-Type: text/html; charset=UTF-8');
        //     wp_mail( $to, $subject, $body, $headers );
        // }

        curl_close($ch);

    };
    
// Stop WC from adjusting the stock qty itself. JDE API should be the only way to adjust stock qty in this set up.

    function prevent_adjust_line_item ( $prevent, $item, $quantity ) {
        $prevent = true;
        return $prevent;
    }
    
    add_filter( 'woocommerce_prevent_adjust_line_item_product_stock', 'prevent_adjust_line_item', 10, 3 );
