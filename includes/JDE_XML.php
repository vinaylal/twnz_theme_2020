<?php

$xmldata = '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:env="http://schemas.twbm.com/osb/common/envelope" xmlns:head="http://shcemas.twbm.com/osb/common/header" xmlns:esh="http://schemas.twbm.com/osb/service/Eship-SalesOrderEDI-I-NZL-3PL" xmlns:mes="http://schemas.twbm.com/osb/common/message">
    <soap:Header 	xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">
        <wsse:Security 	soap:mustUnderstand="1" xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
            <wsse:UsernameToken 	wsu:Id="UsernameToken-IiU35PbAkBQDpS0v0wjkvw22" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">
                <wsse:Username>dev2</wsse:Username>
                <wsse:Password 	Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText">Passw0rd</wsse:Password>
            </wsse:UsernameToken>
        </wsse:Security>
    </soap:Header>
    <soapenv:Body>
        <env:ServiceEnvelope xmlns:env="http://schemas.twbm.com/osb/common/envelope" xmlns:head="http://shcemas.twbm.com/osb/common/header" xmlns:esh="http://schemas.twbm.com/osb/service/Eship-SalesOrderEDI-I-NZL-3PL" xmlns:mes="http://schemas.twbm.com/osb/common/message">
            <env:ServiceHeader>
                <head:CommonDetail>
                    <head:ServiceName>EShip-SalesOrderEDI-I-NZL-3PL</head:ServiceName>
                    <head:RqDateTime>' . $order->get_date_created() . '</head:RqDateTime>
                    <head:RqExpiryTime>3</head:RqExpiryTime>
                    <head:RqNature>
                        <head:RqStatus>WS_REQ</head:RqStatus>
                    </head:RqNature>
                </head:CommonDetail>
                <head:ClientDetail>
                    <head:Organization>TW</head:Organization>
                    <head:SubSystem>Eship-NZL</head:SubSystem>
                    <head:RegionDetail>
                        <head:CountryCode>MYS</head:CountryCode>
                    </head:RegionDetail>
                </head:ClientDetail>
            </env:ServiceHeader>
            <env:ServiceBody>
                <!--You have a CHOICE of the next 3 items at this level-->
                <env:RqDetail>
                    <!--Zero or more repetitions:-->
                    <esh:F47011>
                        <!--ID:-->
                        <esh:SYEDOC>6077</esh:SYEDOC>
                        <!--Type:-->
                        <esh:SYEDCT>S2</esh:SYEDCT>
                        <!--Company:-->
                        <esh:SYEKCO>00034</esh:SYEKCO>
                        <!--Document Key Company:-->
                        <esh:SYKCOO>00034</esh:SYKCOO>
                        <!--Order Type:-->
                        <esh:SYDCTO>ZD</esh:SYDCTO>
                        <!--Business Unit:-->
                        <esh:SYMCU>     3402001</esh:SYMCU>
                        <!--Original Document Company:-->
                        <esh:SYOKCO> </esh:SYOKCO>
                        <!--Original Document Number:-->
                        <esh:SYOORN>30265953</esh:SYOORN>
                        <!--Original Document Type:-->
                        <esh:SYOCTO>ZD</esh:SYOCTO>
                        <!--Address Number:-->
                        <esh:SYAN8>0</esh:SYAN8>
                        <!--Ship To:-->
                        <esh:SYSHAN>0</esh:SYSHAN>
                        <!--Order Date:-->
                        <esh:SYTRDJ>' . $formattedDate . '</esh:SYTRDJ>
                        <!--Schedule Name:-->
                        <esh:SYASN>int</esh:SYASN>
                        <!--Retail Price:-->
                        <esh:SYURAT>17294</esh:SYURAT>
                        <!--Consultant Number:-->
                        <esh:SYRORN>100608</esh:SYRORN>
                        <!--Sales Week:-->
                        <esh:SYURRF>202038</esh:SYURRF>
                        <!--Delivery Number:-->
                        <esh:SYVR01>' . $id . '</esh:SYVR01>
                        <!--DeliveryInstructions1:-->
                        <esh:SYDEL1>' . $order->get_billing_email() . '. ' . $order->get_customer_note() . '</esh:SYDEL1>
                        <!--DeliveryInstructions2:-->
                        <esh:SYDEL2> </esh:SYDEL2>';

                        foreach ( $order->get_items() as $item_id => $item ) {
                            
                            $subt = (float)$item->get_subtotal();
                            $qty = (float)$item->get_quantity();
                            $SZAEXP = ($subt) * ($qty);

	                        $xmldata .= '<!--Optional:-->
	                        <esh:F47012Detail>
	                            <!--Zero or more repetitions:-->
	                            <esh:F47012Detail_Collection>
	                                <!--Line Number:-->
	                                <esh:SZEDLN>1</esh:SZEDLN>
	                                <!--Document Key Company:-->
	                                <esh:SZKCOO> </esh:SZKCOO>
	                                <!--Document Type:-->
	                                <esh:SZDCTO>ZD</esh:SZDCTO>
	                                <!--Original Document Company:-->
	                                <esh:SZOKCO> </esh:SZOKCO>
	                                <!--Original Document Number:-->
	                                <esh:SZOORN>30265953</esh:SZOORN>
	                                <!--Original Document Type:-->
	                                <esh:SZOCTO>ZD</esh:SZOCTO>
	                                <!--Address Number:-->
	                                <esh:SZAN8>0</esh:SZAN8>
	                                <!--Ship To:-->
	                                <esh:SZSHAN>0</esh:SZSHAN>
	                                <!--Order Date:-->
	                                <esh:SZTRDJ>' . $formattedDate . '</esh:SZTRDJ>
	                                <!--Line Number:-->
	                                <esh:SZLNID>1000</esh:SZLNID>
	                                <!--2nd Item Number:-->
	                                <esh:SZLITM>11134564</esh:SZLITM>
	                                <!--U/M:-->
	                                <esh:SZUOM/>
	                                <!--Quantity:-->
	                                <esh:SZUORG>' . $qty . '</esh:SZUORG>
	                                <!--Unit Price:-->
	                                <esh:SZUPRC>' . $subt . '</esh:SZUPRC>
	                                <!--Extended Price:-->
	                                <esh:SZAEXP>' . $SZAEXP . '</esh:SZAEXP>
	                                <!--Line Type:-->
	                                <esh:SZLNTY>S</esh:SZLNTY>
	                                <!--Branch/Plant:-->
	                                <esh:SZMCU>     3402001</esh:SZMCU>
	                                <!--G/L Offset:-->
	                                <esh:SZGLC>PFCR</esh:SZGLC>
	                                <!--Override Price:-->
	                                <esh:SZPROV>1</esh:SZPROV>
	                                <!--Schedule Name:-->
	                                <esh:SZASN/>
	                                <!--Delivery Number:-->
	                                <esh:SZVR01>' . $id . '</esh:SZVR01>
	                                <!--Sales Week:-->
	                                <esh:SZURRF>202038</esh:SZURRF>
	                                <!--Retail Price:-->
	                                <esh:SZURAT>' . $item->get_subtotal() . '</esh:SZURAT>
	                                <!--Consultant Number:-->
	                                <esh:SZRORN>100608</esh:SZRORN>
	                            </esh:F47012Detail_Collection>
	                        </esh:F47012Detail>';
	                	}

	                	$xmldata .=
                        '<!--Optional:-->
                        <esh:F4706Detail>';

                        // Sort some variables based on if shipping details are filled out or not
                        // firstname
	                        if( !empty( $order->get_shipping_first_name() ) ){
	                        	$firstname = $order->get_shipping_first_name();
	                        }else{
	                        	$firstname = $order->get_billing_first_name();
	                        }
                        // secondname
	                        if( !empty( $order->get_shipping_last_name() ) ){
	                        	$lastname = $order->get_shipping_last_name();
	                        }else{
	                        	$lastname = $order->get_billing_last_name();
	                        }
                        // firstaddress
	                        if( !empty( $order->get_shipping_address_1() ) ){
	                        	$firstaddress = $order->get_shipping_address_1();
	                        }else{
	                        	$firstaddress = $order->get_billing_address_1();
	                        }
                        // secondaddress
	                        if( !empty( $order->get_shipping_address_2() ) ){
	                        	$secondaddress = $order->get_shipping_address_2();
	                        }else{
	                        	$secondaddress = $order->get_billing_address_2();
	                        }
                        // city
	                        if( !empty( $order->get_shipping_city() ) ){
	                        	$city = $order->get_shipping_city();
	                        }else{
	                        	$city = $order->get_billing_city();
	                        }
                        // phone
	                    	$phone = $order->get_billing_phone();
                    	// state
	                        if( !empty( $order->get_shipping_state() ) ){
	                        	$state = $order->get_shipping_state();
	                        }else{
	                        	$state = $order->get_billing_state();
	                        }
                        // postcode
	                        if( !empty( $order->get_shipping_postcode() ) ){
	                        	$postcode = $order->get_shipping_postcode();
	                        }else{
	                        	$postcode = $order->get_billing_postcode();
	                        }


                        $xmldata .= '<!--Zero or more repetitions:-->
                            <esh:F4706Detail_Collection>
                                <!--Ship To:-->
                                <esh:ZAAN8>344429</esh:ZAAN8>
                                <!--Document Type:-->
                                <esh:ZADCTO>ZD</esh:ZADCTO>
                                <!--Document Company:-->
                                <esh:ZAKCOO>00034</esh:ZAKCOO>
                                <!--Address Number Type:-->
                                <esh:ZAANTY>1</esh:ZAANTY>
                                <!--Mailing Name:-->
                                <esh:ZAMLNM>' . $firstname . ' ' . $lastname . '</esh:ZAMLNM>
                                <!--Address 1:-->';

                        $xmldata .= '<esh:ZAADD1>' . $firstaddress;

                        if( !empty( $secondaddress ) ){

                        	$xmldata .= ' ' . $secondaddress;
                    	}

                    	$xmldata .= '</esh:ZAADD1>';

                        
                        $xmldata .= '<!--Address 2-->
                                <esh:ZAADD2>' . $city . '</esh:ZAADD2>
                                <!--Address 3-->
                                <esh:ZAADD3></esh:ZAADD3>
                                <!--Telephone / Email Address:-->
                                <esh:ZAADD4>' . $phone . '</esh:ZAADD4>
                                <!--Suburb:-->
                                <esh:ZAACTY1/>
                                <!--State:-->
                                <esh:ZAADDS>' . $state . '</esh:ZAADDS>
                                <!--Country:-->
                                <esh:ZACTR>NZ</esh:ZACTR>
                                <!--Post Code:-->
                                <esh:ZAADDZ>' . $postcode . '</esh:ZAADDZ>
                            </esh:F4706Detail_Collection>';

                        $xmldata .= '</esh:F4706Detail>
                    </esh:F47011>
                </env:RqDetail>
            </env:ServiceBody>
        </env:ServiceEnvelope>
    </soapenv:Body>
</soapenv:Envelope>';