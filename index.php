<?php

/*
 * Template Name: Generic Page
 * description: >-
 */

?>
<?php get_header(); ?>
<?php $adjust_middle_column = false; ?>
<?php $CurrentPage = home_url( add_query_arg( array(), $wp->request ) ); ?>

<?php
    // if( 
    //     $CurrentPage == 'https://dev.twnz.co.nz/cart' || 
    //     $CurrentPage == 'https://dev.twnz.co.nz/checkout' ||
    //     $CurrentPage == 'https://tupperware.twnz.co.nz/cart' ||
    //     $CurrentPage == 'https://tupperware.twnz.co.nz/checkout' ||
    //     $CurrentPage == 'https://twnz.co.nz/cart' ||
    //     $CurrentPage == 'https://twnz.co.nz/checkout' ||
    //     $CurrentPage == 'https://tupperware.co.nz/cart' ||
    //     $CurrentPage == 'https://tupperware.co.nz/checkout'
    // ){
        
    //     $adjust_middle_column = true;
    //     get_template_part( 'template-parts/cart-slider' );
                    
    // }
?>

<div id="content">
    <section class="row">
        <?php
            if( 
                $CurrentPage == 'https://dev.twnz.co.nz/cart' || 
                $CurrentPage == 'https://dev.twnz.co.nz/checkout' ||
                $CurrentPage == 'https://tupperware.twnz.co.nz/cart' ||
                $CurrentPage == 'https://tupperware.twnz.co.nz/checkout' ||
                $CurrentPage == 'https://twnz.co.nz/cart' ||
                $CurrentPage == 'https://twnz.co.nz/checkout' ||
                $CurrentPage == 'https://tupperware.co.nz/cart' ||
                $CurrentPage == 'https://tupperware.co.nz/checkout'
            ){
                
                $adjust_middle_column = true;
                            
            }else{
                ?>
                    <div id="leftColumn" class="col">
                        <div class="wrapper">
                            <div id="leftNavigation">
                                <?php get_template_part( 'template-parts/categories-menu' ); ?>
                            </div>
                        </div>
                    </div>
                <?php
            }
        ?>
        <div id="middleColumn" class="col <?php if($adjust_middle_column){ echo 'adjust_middle_column';} ?>">
            <div class="wrapper">
                
                <?php

                    if ( is_archive() && ! have_posts() ) {
                        $archive_title = __( 'Nothing Found', 'twentytwenty' );
                    } elseif ( ! is_home() ) {
                        $archive_title    = get_the_archive_title();
                        $archive_subtitle = get_the_archive_description();
                    }

                    if ( $archive_title || $archive_subtitle ) {
                        ?>

                        <header class="archive-header has-text-align-center header-footer-group">

                            <div class="archive-header-inner section-inner medium">

                                <?php if ( $archive_title ) { ?>
                                    <h1 class="archive-title"><?php echo wp_kses_post( $archive_title ); ?></h1>
                                <?php } ?>

                                <?php if ( $archive_subtitle ) { ?>
                                    <div class="archive-subtitle section-inner thin max-percentage intro-text"><?php echo wp_kses_post( wpautop( $archive_subtitle ) ); ?></div>
                                <?php } ?>

                            </div><!-- .archive-header-inner -->

                        </header><!-- .archive-header -->

                        <?php
                    }

                    if ( is_home() ){
                        echo '<h1>Blog</h1>';
                    }
                    
                    if ( have_posts() ) {
                        
                        while ( have_posts() ) {
                            
                            the_post();
                            echo '<a href="' . get_permalink() . '">';
                            the_title( '<h1 class="entry-title">', '</h1>' );
                            echo '</a>';
                            echo '<a href="' . get_permalink() . '">';
                            the_post_thumbnail();
                            echo '</a>';
                            the_excerpt();
                            echo '<a href="' . get_permalink() . '">Read More...</a>';
                        }

                        get_template_part( 'template-parts/pagination' );
                    }
                ?>                                
                
            </div>
        </div>
                            
    </section>
</div>
<?php get_footer(); ?>
