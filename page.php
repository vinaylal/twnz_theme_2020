<?php

/*
 * Template Name: Generic Page
 * description: >-
 */

?>
<?php get_header(); ?>
<?php $adjust_middle_column = false; ?>
<?php $CurrentPage = home_url( add_query_arg( array(), $wp->request ) ); ?>

<?php
    // if( 
    //     $CurrentPage == 'https://dev.twnz.co.nz/cart' || 
    //     $CurrentPage == 'https://dev.twnz.co.nz/checkout' ||
    //     $CurrentPage == 'https://tupperware.twnz.co.nz/cart' ||
    //     $CurrentPage == 'https://tupperware.twnz.co.nz/checkout' ||
    //     $CurrentPage == 'https://twnz.co.nz/cart' ||
    //     $CurrentPage == 'https://twnz.co.nz/checkout' ||
    //     $CurrentPage == 'https://tupperware.co.nz/cart' ||
    //     $CurrentPage == 'https://tupperware.co.nz/checkout'
    // ){
        
    //     $adjust_middle_column = true;
    //     get_template_part( 'template-parts/cart-slider' );
                    
    // }
?>

<div id="content">
    <section class="row">
        <?php
            if( 
                $CurrentPage == 'https://dev.twnz.co.nz/cart' || 
                $CurrentPage == 'https://dev.twnz.co.nz/checkout' ||
                $CurrentPage == 'https://tupperware.twnz.co.nz/cart' ||
                $CurrentPage == 'https://tupperware.twnz.co.nz/checkout' ||
                $CurrentPage == 'https://twnz.co.nz/cart' ||
                $CurrentPage == 'https://twnz.co.nz/checkout' ||
                $CurrentPage == 'https://tupperware.co.nz/cart' ||
                $CurrentPage == 'https://tupperware.co.nz/checkout'
            ){
                
                $adjust_middle_column = true;
                            
            }else{
                ?>
                    <div id="leftColumn" class="col">
                        <div class="wrapper">
                            <div id="leftNavigation">
                                <?php get_template_part( 'template-parts/categories-menu' ); ?>
                            </div>
                        </div>
                    </div>
                <?php
            }
        ?>
        <div id="middleColumn" class="col <?php if($adjust_middle_column){ echo 'adjust_middle_column';} ?>">
            <div class="wrapper">

                <?php 
                    if ( have_posts() ) {
                        while ( have_posts() ) {
                            the_title( '<h1 class="entry-title">', '</h1>' );
                            the_post();
                            the_content();
                        } // end while
                    } // end if
                ?>                                
                
            </div>
        </div>
                            
    </section>
</div>
<?php get_footer(); ?>