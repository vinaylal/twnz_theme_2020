<?php

/*
 * Template Name: Search Page
 * description: >-
 */

?>

<?php get_header(); ?>
                    
<div id="content">
    <section class="row">
    
        <div id="leftColumn" class="col">
            <div class="wrapper">
                <div id="leftNavigation">
                    <?php get_template_part( 'template-parts/categories-menu' ); ?>
                </div>                
            </div>
        </div>
    
        <div id="middleColumn" class="col">
            <div class="wrapper">
                
                <h1>Search</h1>
                
                <div class="contentRow  dottedBottom">
                    <div class="clr"></div>
                </div>
                
                <div id="searchResult" class="searchResult">
                    <?php if ( have_posts() ) : ?>
                        
                        <header class="page-header">
                            <h1 class="page-title">
                            <?php
                                /* translators: %s: search query. */
                                printf( esc_html__( "Showing results for '%s'", 'buddyboss-theme' ), '<span>' . get_search_query() . '</span>' );
                            ?>
                            </h1>
                        </header><!-- .page-header -->

                        <div class="post-grid <?php echo esc_attr( $class ); ?>">
                            <?php
                            /* Start the Loop */
                            while ( have_posts() ) :
                                the_post();
                            ?>
                                
                            <div class="contentRow dottedBottom">
                                <div>
                                    <?php if( has_post_thumbnail() ){ ?>
                                        <span class="picture" style="width: 109px;">
                                            <a href="<?php the_permalink(); ?>">
                                                <img alt="" src="<?php the_post_thumbnail_url(); ?>"  style="">
                                            </a>
                                        </span>
                                    <?php } ?>
                                    <h3 data-initialized="true">
                                        <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                                    </h3>
                                    <span class="fragment">
                                        <?php the_excerpt(); ?>
                                        <p><a href="<?php the_permalink(); ?>" class="more">Learn more</a></p>
                                    </span>
                                </div>
                                <p class="clr"></p>
                            </div>

                            <?php
                            endwhile;
                            ?>
                        </div>

                        <?php

                    else :
                        ?> <p>No search results for your phrase. Please try again.<p>

                    <?php endif; ?>
                </div>
                
            </div>
        </div>
                            
    </section>
</div>

<?php get_footer(); ?>