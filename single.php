<?php

/*
 * Template Name: Single Post Page
 * description: >-
 */

?>
<?php get_header(); ?>
<?php 
    $thumb_id = get_post_thumbnail_id();
    $thumb_url_array = wp_get_attachment_image_src($thumb_id, 'thumbnail-size', true);
    $thumb_url = $thumb_url_array[0];
?>
<header class="intro-header" style="color: #fff; background-color: #1eb5da; background: url('<?php echo $thumb_url; ?>') no-repeat center center fixed; -webkit-background-size: cover; -moz-background-size: cover; -o-background-size: cover; background-size: cover;">
    <div class="blog-entry-title">
        <?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
        <?php get_template_part( 'template-parts/entry-author-bio' ); ?>
        <?php
            $taxonomy = 'category';

            // Get the term IDs assigned to post.
            $post_terms = wp_get_object_terms( $post->ID, $taxonomy, array( 'fields' => 'ids' ) );
             
            // Separator between links.
            $separator = '/ ';
             
            if ( ! empty( $post_terms ) && ! is_wp_error( $post_terms ) ) {
             
                $term_ids = implode( ',' , $post_terms );
             
                $terms = wp_list_categories( array(
                    'title_li' => '',
                    'style'    => 'category_link',
                    'echo'     => false,
                    'taxonomy' => $taxonomy,
                    'include'  => $term_ids
                ) );
             
                $terms = rtrim( trim( str_replace( '/',  $separator, $terms ) ), $separator );
             
                // Display post categories.
                echo  '<div class="author_bio_section"><p>Posted in : ' . $terms . '</p></div>';
            }
        ?>
    </div>

</header>

<div id="content" data-test="vinay">
    <section class="row">
        <div id="leftColumn" class="col">
            <div class="wrapper">
                <div id="leftNavigation">
                    <h3>Recent Posts:</h3>
 
                        <?php 
                        // Define our WP Query Parameters
                        $the_query = new WP_Query( 'posts_per_page=5' ); ?>
                          
                         
                        <?php 
                        // Start our WP Query
                        while ($the_query -> have_posts()) : $the_query -> the_post(); 
                        // Display the Post Title with Hyperlink
                            $recent_thumb_id = get_post_thumbnail_id();
                            $recent_thumb_url_array = wp_get_attachment_image_src($recent_thumb_id, 'thumbnail-size', true);
                            $recent_thumb_url = $recent_thumb_url_array[0];
                        ?>
                        
                        <a class="recentPost" href="<?php the_permalink() ?>">
                            <img src="<?php echo $recent_thumb_url; ?>" alt="<?php the_title(); ?>" />
                            <h2><?php the_title(); ?></h2>
                        </a>
                          
                         
                        <?php 
                        // Display the Post Excerpt
                        the_excerpt(__('(more…)')); ?>
                          
                         
                        <?php 
                        // Repeat the process and reset once it hits the limit
                        endwhile;
                        wp_reset_postdata();
                        ?>

                </div>
            </div>
        </div>
        <div id="middleColumn" class="col">
            <div class="wrapper">

                <?php 
                    if ( have_posts() ) {
                        while ( have_posts() ) {
                            the_post();
                            the_content();
                        }
                    }
                    
                    echo '<hr>';
                    get_template_part( 'template-parts/author-bio' );
                ?>    
                <hr>
                <?php comments_template(); ?>
            </div>
        </div>
                            
    </section>
</div>
<?php get_footer(); ?>