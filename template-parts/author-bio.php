<?php

global $post;
$authorMobile = new Mobile_Detect;

// Detect if it is a single post with a post author
if ( is_single() && isset( $post->post_author ) ) {
  
    $display_name = get_the_author_meta( 'display_name', $post->post_author );
    if ( empty( $display_name ) ){
    	$display_name = get_the_author_meta( 'nickname', $post->post_author );
    }
    $description = get_the_author_meta( 'description', $post->post_author );
    $avatar = get_avatar( get_the_author_meta( 'ID' ), 180 );
    $email = get_the_author_meta( 'email', $post->post_author );
    $facebook = get_the_author_meta( 'facebook', $post->post_author );
    $instagram = get_the_author_meta( 'instagram', $post->post_author );
    $billing_phone = get_the_author_meta( 'billing_phone', $post->post_author );
    $url = get_the_author_meta( 'url', $post->post_author );

    $author_details = '<div class="largeAuthorBio">';

    if( $avatar ) { $author_details .= $avatar; }
    
    $author_details .= '<div class="authorDetails">';
    $author_details .= '<h2>' . $display_name . '</h2>';
    $author_details .= '<p>' . $description . '</p>';

    $author_details .= '<div class="authorContacts">';
    if( $email ) { $author_details .= '<a href="mailto:' . $email . '"><i class="fas fa-envelope-square"></i></a>'; }
    if( $facebook ) { $author_details .= '<a href="' . $facebook . '"><i class="fab fa-facebook-square"></i></a>'; }
    if( $instagram ) { $author_details .= '<a href="' . $instagram . '"><i class="fab fa-instagram"></i></a>'; }
    if( $billing_phone ) { 

    	if( $authorMobile->isMobile() ) { 
    		$author_details .= '<a href="tel:' . $billing_phone . '"><i class="fas fa-phone-square"></i></a>';
		}else{
			$author_details .= '<a href="tel:' . $billing_phone . '">' . $billing_phone . '</a>';
		}
    }
    if( $url ) { $author_details .= '<a href="' . $url . '"><i class="fas fa-money-check-alt"></i></a>'; }
    
    $author_details .= '</div>';

    $author_details .= '</div>';
    
    $author_details .= '</div>';
}

echo $author_details;