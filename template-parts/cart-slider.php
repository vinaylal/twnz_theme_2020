<?php
$detect = new Mobile_Detect;
// Check rows exists.
if( have_rows('cart_slider_wrapper') ):
    ?>
        <div class="flexslider">
        <ul class="slides">
    <?php
    // Loop through rows.
    while( have_rows('cart_slider_wrapper') ) : the_row();
        
        if( have_rows('slide') ):
            while( have_rows('slide') ): the_row();

                $file = get_sub_field('image_desktop');
                $imageMobile = get_sub_field('image_mobile');
                $link = get_sub_field('link');
                $linkText = get_sub_field('link_text');
                $tagLine = get_sub_field('tag_line');
            ?>

            <li>
                
                <?php if( $file['type'] == 'image' ) { ?>

                    <?php if ( $detect->isMobile() ) { ?>
                    
                        <img src="<?php echo esc_url( $imageMobile['url'] ); ?>" />

                    <?php }else{ ?>
                        
                        <img src="<?php echo esc_url( $file['url'] ); ?>" />

                    <?php } ?>

                <?php }elseif ($file['type'] == 'video') { ?>
                    
                    <video style="width:100%;" src="<?php echo esc_url( $file['url'] ); ?>" ></video>

                <?php } ?>

                <?php if(!empty($tagLine)){ ?>
                    <h2><?php echo $tagLine; ?></h2>
                <?php } ?>

                <?php if(!empty($linkText)){ ?>
                    <a href="<?php echo $link; ?>" onclick="ga('send', 'event', 'Home Facelift', 'Button A', 'Learn More');" data-initialized="true">
                        <button class="button"><?php echo $linkText; ?></button>
                    </a>
                <?php } ?>

            </li>

            <?php endwhile;
        endif;

    // End loop.
    endwhile;
    ?>
    </ul>
    </div>
    <?php
// No value.
else :
    // Do something...
endif;
    