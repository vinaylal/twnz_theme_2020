<?php

if (isset($_GET['cons'])) {
  	
  	$args = array(
		'meta_key'		=> 'consultant_number',
		'meta_value'	=> sanitize_text_field ( $_GET['cons'] )
	);

    $wp_user_query = new WP_User_Query( $args );
    $consultants = $wp_user_query->get_results();

    foreach( $consultants as $consultant ){
		?>

			<div class="toTopButtonPanel">
                <a class="icon arrowTop" href="/shop/">You are shopping with <?php echo $consultant->display_name; ?> <i class="fas fa-angle-double-right"></i></a>
            </div>

	    <?php
    }
    

} else {
  
  ?>

	<div class="toTopButtonPanel">
        <a class="icon arrowTop" href="/connect/">Find your nearest Consultant <i class="fas fa-angle-double-right"></i></a>
    </div>

  <?php
}