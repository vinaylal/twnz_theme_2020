<?php

global $post;
  
// Detect if it is a single post with a post author
if ( is_single() && isset( $post->post_author ) ) {
  
    $display_name = get_the_author_meta( 'display_name', $post->post_author );
    if ( empty( $display_name ) ){
    	$display_name = get_the_author_meta( 'nickname', $post->post_author );
    }
    $author_details = '<div class="author_bio_section"><p class="author_name">';

    $author_details .= ' By ' . $display_name . ' <a href="'. esc_url( get_author_posts_url( get_the_author_meta( 'ID' ) ) ) .'">, View all posts by ' . $display_name . '</a></p></div>';
}

echo $author_details;