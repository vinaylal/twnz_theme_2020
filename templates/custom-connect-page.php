<?php

/*
 * Template Name: Connect Page
 * description: >-
 */

?>

<!DOCTYPE html>
<html lang="en" class="home connect  ">
    <head><!-- PageID 1278 -->
        <!-- META -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
        <meta name="author" content="" />
        <meta name="description" content="" />
        <meta name="keywords" content="" />
        <meta name="robots" content="index,follow" />
        <meta property="og:title" content="Connect | Tupperware" />
<meta property="og:type" content="article" />
<meta property="og:image" content="https://www.tupperware.com.au/assets/system_images/connect-logo-fb.png" />
<meta property="og:url" content="https://www.tupperware.com.au/connect" />
<meta property="og:description" content="Find the Tupperware party manager nearest you." />



        <title>Find a consultant</title>
        
        
        
        <style>
            html{-webkit-text-size-adjust:none}
            body{-webkit-font-smoothing:antialiased}
            body,html,div,img,ul,form,header,section{margin:0;padding:0;-webkit-box-sizing:border-box;-moz-box-sizing:border-box;-ms-box-sizing:border-box;box-sizing:border-box}
            body,input{font-family:TupperwarePro Light,Arial,Helvetica,Sans-Serif;font-size:16px;line-height:1.5em;color:#302a26}
            body{background:#f3f3f7;color:#302a26;position:relative}
            img{border:none}
            p{margin:0 0 1em;padding:0}
            h1{margin:31px 0 10px;padding:0}
            #notification{display:none}
            #base{width:100%;overflow:hidden;position:relative;z-index:1;opacity:1}
            #cover{width:100%;position:relative;z-index:10;overflow:hidden}
            #page{width:100%;position:relative;background:#fff}
            header{width:100%;position:absolute;top:0;left:0;z-index:1000;height:101px;padding:13px 64px 0;background:#fff;-moz-box-shadow:0 2px 2px 0 rgba(0,0,0,0.1);-webkit-box-shadow:0 2px 2px 0 rgba(0,0,0,0.1);-ms-box-shadow:0 2px 2px 0 rgba(0,0,0,0.1);box-shadow:0 2px 2px 0 rgba(0,0,0,0.1)}
            #content{width:100%;position:relative;z-index:500;background:#fff;padding-top:101px}
            .contentPanel > .contentRow{width:100%;max-width:1582px;padding:0;margin:0 auto}
            .clr{display:block;font-size:0!important;line-height:0!important;clear:both!important;float:none!important;padding:0!important;margin:0!important;height:0!important;overflow:hidden!important}
            .right{float:right}
            .icon{position:relative;display:inline-block;padding:7px 0 7px 35px;line-height:1.2em}
            .icon:before{content:"";display:block;position:absolute;width:27px;height:27px;top:0;left:0;background:url(/static/tupperware-master/assets/system_images/connect-sprite-icons.png) 0 0 no-repeat}
            .icon.arrowTop{background:url(/static/tupperware-master/assets/system_images/connect-sprite-icons.png) -9px -369px no-repeat;color:#000;display:inline-block;font-size:16px;height:34px;line-height:1em;padding:40px 0 0;text-align:center;text-decoration:none;text-transform:uppercase;width:74px}
            .icon.arrowTop:before{display:none}
            h1.ultralight{color:#000;font-size:50px;line-height:1.5em;font-family:TupperwarePro Ultralight,Arial,Helvetica,Sans-Serif;text-align:center;font-weight:400}
            .fixedBottomControlPanel{list-style:none;bottom:20px;position:fixed;right:25px;z-index:600}
            form{padding:0;margin:0}
            form,input{font-size:15px;line-height:1.3em}
            input{-webkit-box-sizing:border-box;-moz-box-sizing:border-box;-ms-box-sizing:border-box;box-sizing:border-box;-webkit-resize:none;-moz-resize:none;-ms-resize:none;resize:none;outline:none}
            input[type="text"]{position:relative;z-index:10;width:100%;padding:.3em .4em;margin:0 0 1.5em;border:1px solid #fff;background:#fff;color:#302a26;opacity:1}
            input[type="submit"]{display:inline-block;height:34px;border:1px solid #1ab5da;background-color:#1ab5da;font-weight:400;color:#fff;padding:0 5em 3px;margin:2px .25em 1em;font-size:18px;border-radius:0;-webkit-appearance:none}
            #logo{float:left;width:233px;padding:5px 0 0}
            #logo img{display:block;width:100%}
            #logo a{outline:none}
            #topPanel{float:right;padding-top:28px;z-index:1000}
            #topPanel ul li{margin:0 0 0 40px}
            ul.buttonsLine{list-style:outside none none;margin:0;padding:0;text-align:center;width:100%}
            ul.buttonsLine > li{float:none;display:inline-block;margin:0 20px 1em;background:#fff}
            ul.buttonsLine > li > a{color:#333;padding:7px 0 3px;border:1px solid #999;line-height:1em;text-align:center;display:block;height:24px;min-width:261px;text-decoration:none;background:#fff;font-size:16px}
            @keyframes transformButtonsRev {
            from{height:20px;min-width:214px;font-size:14px}
            to{height:24px;min-width:261px}
            }
            #topPanel ul.buttonsLine > li > a{padding:7px 0 3px;line-height:1em;text-align:center;display:block;height:24px;min-width:261px;text-decoration:none;animation-name:transformButtonsRev;animation-duration:.5s}
            #topPanel ul.buttonsLine > li > a.blue{color:#fff;border:1px solid #1ab5da;background:#1ab5da}
            header .searchPanel{display:none;width:auto;height:35px;float:left;margin:24px 10px 0 15px;border:none}
            .searchFormTop{border:1px solid #e0e0e0;border-radius:15px;-webkit-border-radius:15px;-moz-border-radius:15px;-ms-border-radius:15px;position:relative;padding:3px 8px;background:#fff;float:left}
            .searchFormTop input{border:none;background:transparent;outline:none;-moz-box-shadow:none;-webkit-box-shadow:none;-ms-box-shadow:none;box-shadow:none}
            .searchFormTop input[type="text"]{-webkit-box-sizing:border-box;-moz-box-sizing:border-box;-ms-box-sizing:border-box;box-sizing:border-box;width:100%;padding:0 25px 0 7px;margin:0;float:left;font-size:15px;font-family:TupperwarePro Light,​Arial,​Helvetica,​sans-serif;color:#666;line-height:1em;height:25px;position:relative;z-index:10;width:200px}
            .searchFormTop input[type="image"]{width:20px;height:20px;position:absolute;z-index:20;top:7px;right:10px;background:url(/static/tupperware-master/assets/system_images/connect-sprite-icons.png) -167px -17px no-repeat #fff}
            .searchFormTop input[type="text"]::-webkit-input-placeholder{font-size:15px;font-family:TupperwarePro Light,​Arial,​Helvetica,​sans-serif;color:#666;font-style:normal;line-height:normal;opacity:.5}
            .searchFormTop input[type="text"]::-moz-placeholder{font-size:15px;font-family:TupperwarePro Light,​Arial,​Helvetica,​sans-serif;color:#666;font-style:normal;line-height:normal;opacity:.5}
            .searchFormTop input[type="text"]:-ms-input-placeholder{font-size:15px;font-family:TupperwarePro Light,​Arial,​Helvetica,​sans-serif;color:#666;font-style:normal;line-height:normal;opacity:.5}
            .searchFormSwitch{display:none;margin:0 0 0 10px;padding:12px 0 12px 10px;width:60px;height:40px;border-left:2px solid #ccc;float:right;position:relative}
            .searchFormSwitch:after{content:"";position:absolute;display:block;width:55px;height:55px;position:absolute;left:10px;top:6px}
            .searchFormSwitch span{display:none}
            .socialMedia ul li a span{display:none}
            .stage .placeholder{position:relative;width:100%;min-height:400px;background:url(/static/tupperware-master/assets/system_images/ajax-loader.gif) center 150px no-repeat;overflow:hidden}
            .stage .aligner{display:table;width:100%;height:100%;position:absolute;opacity:0;top:0;left:0}
            .stage .aligner .cell{display:table-cell;height:300px;width:100%;text-align:center;vertical-align:middle;position:relative;padding:1em}
            #content > .row > .stage .aligner .cell .row{width:100%;max-width:643px;padding:28px 20px 15px;background-color:rgba(255,255,255,0.7);margin:0 auto}
            .stage .aligner .cell h1{font-family:TupperwarePro Light;font-weight:400;font-size:60px;line-height:1em;color:#333;padding:0;margin:0 0 15px;text-align:center}
            .stage .aligner .cell h2{font-family:TupperwarePro Light;font-weight:400;font-size:20px;line-height:1em;color:#000;padding:0;margin:0 0 30px;text-align:center}
            .stage .aligner .cell .col{padding:0;width:100%;float:none}
            .stage a{text-decoration:none}
            .foot{width:100%;height:auto;text-align:center;font-size:22px;font-family:TupperwarePro Light,Arial,Helvetica,Sans-Serif;line-height:30px;color:#fff;padding:20px 1em}
            .foot span{padding:5px 0;display:inline-block;position:relative}
            .foot span.counter{font-size:38px;line-height:1em;font-family:TupperwarePro Medium,Arial,Helvetica,Sans-Serif;padding:3px 15px 7px;background-color:#fff;color:#000;margin:0 20px;min-width:110px}
            .foot.green{background-color:#09b3ae}
            .stage a{font-family:TupperwarePro Light,Arial,Helvetica,Sans-Serif;color:#666;font-size:14px;line-height:1em;text-decoration:underline;padding:0;margin:0;text-align:center;display:inline-block;width:100%}
            #gmap{width:100%;height:500px;border:1px solid #E5E4E0;margin:0;position:relative}
            .stage .gmapSearch{text-align:center;display:block}
            .stage .gmapSearch.ready{display:none}
            .stage .gmapSearch input[type="text"]{font-size:25px;font-family:TupperwarePro Light,​Arial,​Helvetica,​sans-serif;font-style:normal;display:inline-block;height:51px;max-width:413px;width:100%;padding:0 20px 3px;margin:0 0 30px;background:#fff;border:none;color:#666;opacity:1;-moz-border-radius:0;-webkit-border-radius:0;-ms-border-radius:0;border-radius:0;-moz-box-shadow:none;-webkit-box-shadow:none;-ms-box-shadow:none;box-shadow:none}
            .stage .gmapSearch input[type="text"]:-webkit-input-placeholder{font-size:25px;font-family:TupperwarePro Light,​Arial,​Helvetica,​sans-serif;color:#666;font-style:normal}
            .stage .gmapSearch input[type="text"]:-moz-placeholder{font-size:25px;font-family:TupperwarePro Light,​Arial,​Helvetica,​sans-serif;color:#666;font-style:normal}
            .stage .gmapSearch input[type="text"]:-ms-input-placeholder{font-size:25px;font-family:TupperwarePro Light,​Arial,​Helvetica,​sans-serif;color:#666;font-style:normal}
            .stage .gmapSearch input[type="submit"]{font-size:25px;display:inline-block;height:51px;width:160px;background-color:#1ab5da;font-weight:400;color:#fff;padding:0 0 3px;margin:0 0 1em;line-height:30px;border:none}
            .stage .gmapSearch.ready input[type="submit"]{width:268px;font-size:30px}
            .jcarousel-wrapper{position:relative}
            .jcarousel{position:relative;overflow:hidden;width:100%;max-width:936px}
            .jcarousel ul{width:20000em;position:relative;list-style:none;margin:0;padding:0}
            .partyManager{background:#fff none repeat scroll 0 0;padding:0 20px 1em;position:relative;z-index:1;-moz-box-shadow:0 2px 2px 0 rgba(0,0,0,0.1);-webkit-box-shadow:0 2px 2px 0 rgba(0,0,0,0.1);-ms-box-shadow:0 2px 2px 0 rgba(0,0,0,0.1);box-shadow:0 2px 2px 0 rgba(0,0,0,0.1);width:100%;visibility:visible;position:relative}
            .partyManagerMap{background:#fff none repeat scroll 0 0;position:relative;z-index:1;width:100%;visibility:visible;position:relative}
            .partyContactWrap{box-sizing:border-box;margin:10px auto 0;position:relative;width:100%;max-width:1056px;padding:0 59px}
            .partyContactWrap .partyContactSelector{display:block;list-style-type:none;margin:0 auto;padding:0;overflow:hidden}
            .nextButton,.prevButton{display:block;position:absolute;top:95px;z-index:101;border:1px solid #ccc;border-radius:29px;height:59px;width:59px;background:url(/static/tupperware-master/assets/system_images/connect-sprite-icons.png) no-repeat scroll 3px -215px #fff;text-decoration:none}
            .prevButton{left:0}
            .nextButton{right:0;background-position:-43px -215px}
            .partyManagerDetails{display:block;overflow:hidden;max-height:0}
            .icon{position:relative;display:inline-block;padding:7px 0 7px 35px;line-height:1.2em}
            .icon:before{content:"";display:block;position:absolute;width:27px;height:27px;top:0;left:0;background:url(/static/tupperware-master/assets/system_images/connect-sprite-icons.png) 0 0 no-repeat}
            .notificationBox{border-top:1px solid #fff;border-bottom:1px solid #fff;background:rgba(255,255,255,0.95);color:#7d7d7e;padding:13px 0 16px;margin:1px 0;font-size:18px;line-height:1.3em;position:relative;z-index:100;box-shadow:4px 0 4px rgba(0,0,0,0.4);-moz-box-shadow:4px 0 4px rgba(0,0,0,0.4);-webkit-box-shadow:4px 0 4px rgba(0,0,0,0.4);-ms-box-shadow:4px 0 4px rgba(0,0,0,0.4)}
            .notificationBox .wrapper{padding:0 1em}
            .notificationBox .left{float:left}
            .notificationBox .right{float:right}
            .notificationBox a{color:#0f0701;text-decoration:none;font-size:18px;line-height:1em;position:relative}
            .notificationBox a:hover{color:#a8a6a5;text-decoration:none}
            .notificationBox .closeButton{padding-right:25px}
            .notificationBox .closeButton:after{content:"";display:block;position:absolute;width:18px;height:18px;top:6px;right:0;background:url(/static/tupperware-master/assets/system_images/connect-sprite-icons.png) -318px -18px no-repeat}
            .notificationBox .closeButton:hover:after{opacity:.5}
            .notificationBox p{margin:0}
            .notificationBox.floating{background:rgba(255,255,255,0.95);color:#7d7d7e;border:1px solid rgba(255,255,255,0.95);-webkit-border-radius:4px;-moz-border-radius:4px;-ms-border-radius:4px;border-radius:4px;position:absolute;z-index:550;top:335px;left:100px;padding:25px 28px;width:375px;box-shadow:4px 0 4px rgba(0,0,0,0.4);-moz-box-shadow:4px 0 4px rgba(0,0,0,0.4);-webkit-box-shadow:4px 0 4px rgba(0,0,0,0.4);-ms-box-shadow:4px 0 4px rgba(0,0,0,0.4);font-size:18px;line-height:1.3em}
            .notificationBox.floating .wrapper{padding:0}
            .notificationBox.floating .left,.notificationBox.floating .right{float:none}
            .notificationBox.floating .closeButton{-webkit-box-sizing:border-box;-moz-box-sizing:border-box;-ms-box-sizing:border-box;box-sizing:border-box;margin-top:1em;display:block;width:100%;padding:5px 10px;background:rgba(0,0,0,0.16);-webkit-border-radius:3px;-moz-border-radius:3px;-ms-border-radius:3px;border-radius:3px}
            .notificationBox.floating .closeButton:after{content:"";display:block;position:absolute;width:18px;height:18px;top:6px;right:8px;background:url(/static/tupperware-master/assets/system_images/connect-sprite-icons.png) -318px -18px no-repeat}
            .notificationBox.floating .closeButton:hover:after{opacity:.5}
            .notificationBox.floating a{color:#0f0701;text-decoration:none;font-size:18px;line-height:1em;position:relative}
            .notificationBox.floating a:hover{color:#a8a6a5;text-decoration:none}
            .notificationBox.floating p{margin:0}
            html.addition-head header .foot{opacity:0}
        </style>
        
        <script type="text/javascript">
            var displayWindowsData = new Array();
            var iScrollerData = new Array();
            var beforeInit = new Array();
            var afterInit = new Array();
            
			var youTubePlayers = [];
			var youTubeVideo = [];
			var onYouTubeIframeAPIReady = function () {
				for(var i=0; i<youTubeVideo.length;i++){
					if (typeof(youTubeVideo[i])=='function') youTubeVideo[i].apply();
				}
			}            
        </script>
        
        

        <link type="image/x-icon" rel="shortcut icon" href="/assets/system_images/favicon-connect.ico">
        <link rel="apple-touch-icon" sizes="152x152" href="/assets/system_images/tupperware-touch-icon.png">

        <!--[if lte IE 7]>
        <link rel="stylesheet" media="screen" type="text/css" href="/static/tupperware-au/en/screen_lte_ie7.css">
        <![endif]-->
        
        <!--[if lt IE 9]>
        <script type="text/javascript" src="/static/tupperware-au/en/html5.js"></script>        
        <![endif]-->
        
        <!-- /static/tupperware-au/en/rdm.css, /en/rdm.js -->

        <script type="text/javascript">
			var analyticalToolsToLoad = [];
			var marketingToolsToLoad = [];
			var bufferScarabQueue = [];
        </script>

        <script type="text/javascript">
            window.dataLayer = window.dataLayer || [];
        </script>
        
        

                <!--Google Analytics Code Start-->
        <script>
     
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
            ga('create', 'UA-33180844-10', 'auto');
         
            ga('send', 'pageview');
     
        </script>
        <!--Google Analytics Code End-->

<!-- Google Tag Manager --> <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start': new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0], j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src= 'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f); })(window,document,'script','dataLayer','GTM-NB7GZ8X');</script> <!-- End Google Tag Manager -->
 



        
    </head>

    <body>

        
        
        <!-- Google Tag Manager (noscript) --> <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-NB7GZ8X" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript> <!-- End Google Tag Manager (noscript) -->
        
        <!--[if lt IE 8]>
        <div class="supportWarning">Your Internet Explorer version is not supported by this website. Please upgrade to Internet Explorer 8 (or above) or use another browser (Firefox, Chrome...).</div>
        <![endif]-->
        <noscript><div class="supportWarning">To get the full access to this website, you should activate JavaScript in your browser options.</div></noscript>
        
        
        <div id="base">
            <div id="cover">
                <div id="page">
                    
                    <header class="stickControl">
                        <section class="row">
                            <div class="contentPanel">
                                <div class="contentRow">
                                    
                                    <div class="mobile-navigation">
                                        <a href="javascript:void(0)" class="mobile-navigation-menu" onclick="mobileNavigation.show();"><span></span><span></span><span></span></a>                                        
                                        <div class="clr"></div>
                                    </div>
                                    <div id="logo"><a href="/index"><img src="/assets/system_images/connect-logo.png" alt="Connect" /></a></div>
                                    <div class="searchPanel">
                                        <div class="searchFormTop">
                                            <form action="javascript:void(0)" class="content_form gmapSearch" name="formular-top" onsubmit="unitManagerMap.searchAddress(this.address.value);mobileSearchPanel.toggle(); return false;">
                                                <input type="image" alt="Connect" src="/assets/system_images/trans.png" name="submit">
                                                <input type="text" placeholder="Search ..." value="" name="address" data-type="address" />
                                                <div class="clr"></div>
                                            </form>
                                        </div>
                                        <a class="searchFormSwitch" href="javascript:void(0)"><span>Find a consultant</span></a>
                                        <div class="clr"></div>
                                    </div>
                                    <div id="topPanel">
                                        <ul class="buttonsLine">
                                            
                                            <li><a href="javascript:void(0)" onclick="unitManagerMap.goToMap();ga('send', 'event', 'Connect main page', 'Click on button', 'Find a consultant')">Find a consultant</a></li>
                                            <li rel="mobile-navigation-item"><a class="blue" href="/connect/login.html" target="_blank" onclick="ga('send', 'event', 'Connect main page', 'Click on button', 'Consultant login')">Consultant login</a></li>
                                            
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="clr"></div>
                        </section>
                        
                    </header>
                    
                    <div id="content">
                        <section class="row">
                            
                            


                            <div class="stage main" data-type="picture">
                                <div class="placeholder" data-image-portrait="/assets/images/mediacenter-images/066354-Pantry-Storage_935x1146-revise.jpg" data-image-landscape="/assets/images/mediacenter-images/066174-Pantry-Storage_1664x796.jpg">
                                        <div class="aligner">
                                            <div class="cell">
                                                <div class="col">
                                                    <div class="row">
                                                        
                                                        <h1>Shop Now With Tupperware!</h1>
                                                        <h2>Find A Consultant</h2>
                                                        <form onsubmit="unitManagerMap.searchAddress(this.address.value);return false;" class="content_form gmapSearch " name="formular" method="get" action="javascript:void(0)">
                                                            <input type="text" placeholder="Your Suburb Or City" value="" name="address" data-type="address" /><input type="submit" name="submitBT" value="Search" onclick="ga('send', 'event', 'Connect main page', 'Click on main Connect button', 'User is not geolocated');" />
                                                        </form>
                                                        <form onsubmit="unitManagerMap.goToMap();return false;" class="content_form gmapSearch ready" name="formular-stage" method="get" action="javascript:void(0)">
                                                            <input type="submit" name="submitBT" value="Search" onclick="ga('send', 'event', 'Connect main page', 'Click on main Connect button', 'User is geolocated');"  />
                                                        </form>
                                                        <a href="javascript:void(0);" title="How It Works" onclick="app.scrollToHowItWorks();">How It Works</a>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                </div>
                            </div>
                        



                            <div class="foot green animated" data-type="down">
                                <span class="prefix">At this moment there are</span><span id="myTargetElement" data-start="20468" data-end="22100" class="counter">21056</span><span class="suffix">parties happening</span>
                            </div>
                        </section>

                        <!-- -->


                        <section data-anchor="How can we help you?" class="row" id="howItWorks">
                            <div class="stage" data-type="picture">
                                <div class="placeholder" 
                                    data-image-portrait="/assets/images/mediacenter-images/066318-OLG-Home-Page_935x1146_faded.jpg" 
                                    data-image-landscape="/assets/images/mediacenter-images/066312-OLG-Home-Page_1664x796_faded.jpg">
                                    <div class="aligner">
                                        <div class="cell">
                                            <div class="row threeColumn">
                                                <h1 class="ultralight">How It Works</h1>
                                                <!-- -->
                                                <div class="col">

                                                    <div class="colTeaser">
                                                            <span class="picture"><img alt="" class="lazy-scrolling" data-width="120" data-height="120" data-src="/assets/images/Icon_buy_rdax_120x120.png" /></span>
                                                            <h3>Shop Your Way</h3>
                                                            <p><p>Connect with a consultant below for<br />
a link to shop online or book a party.<br />
&nbsp;</p></p>                                                            
                                                    </div>
                                                </div><!-- -->
                                                <div class="col">

                                                    <div class="colTeaser">
                                                            <span class="picture"><img alt="" class="lazy-scrolling" data-width="120" data-height="120" data-src="/assets/images/Icon_truck_png_rdax_120x120.png" /></span>
                                                            <h3>Deliver To You</h3>
                                                            <p><p>We now deliver direct to your door<br />
so you no longer&nbsp;have to wait!</p></p>                                                            
                                                    </div>
                                                </div><!-- -->
                                                <div class="col">

                                                    <div class="colTeaser">
                                                            <span class="picture"><img alt="" class="lazy-scrolling" data-width="120" data-height="120" data-src="/assets/images/Icon_business_rdax_120x120.png" /></span>
                                                            <h3>Start A Business</h3>
                                                            <p><p>Love our products? Share&nbsp;with others<br />
and earn great income.</p></p>                                                            
                                                    </div>
                                                </div>
                                                <div class="clr"></div>
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </section>
                        <!-- -->
 


                        <section data-anchor-intern="mapTop"></section>
                        
                        <section class="row partyManagerDetails" id="partyManagerDetails"></section>
                        

                        <script type="text/x-tmpl" id="managerDetailsMain">
                        
                            <div class="sectionWrap">
                                <div class="topPanel">
                                    <div class="cols">
                                        <div class="col-1">
                                            <figure class="image round">
                                                <img src="{%=o.picture%}" alt="" width="113" height="119" />
                                            </figure>
                                        </div>
                                        <div class="col-2">
                                        
                                            {% if (o.applicationMode == 'machparty') { %}
                                            <p class="name">Hello, I am {%=o.firstName%}.</p> 
                                            {% if (o.type != 'DS') { %}
                                            <p class="position">I make party with you.</p>    
                                            {% } else { %}
                                                {% if (o.gender == 2) { %}
                                                <p class="position">I am a distributorship.</p>    
                                                {% } else { %}
                                                <p class="position">I am a distributorship.</p>    
                                                {% } %}
                                            {% } %}
                                            {% } else { %}
                                            <p class="name">{%=o.firstName%} {%=o.lastName%}</p>
                                            <p class="position">{%=o.positionName%}</p>
                                            {% } %}
                                            
                                            <div class="withDropdownPanel share">
                                                <a href="javascript:void(0)" class="shareButton"><span>{%=o.labelShare%}</span></a>
                                                <div style="display: none;" class="dropdownPanel">
                                                    <ul class="wrapper">
                                                        <li><a target="_blank" href="mailto:?body={%=o.mailBody%}" rel="mail" class="icon mail" title="Mail"></a></li>
                                                        <li><a href="javascript:void(0)" onclick="unitManagerMap.showShareLink('{%=o.linkURL%}')" rel="link" class="icon link" title="Link"></a></li>
                                                        <li><a target="_blank" href="https://www.facebook.com/sharer/sharer.php?{%=o.facebookShareParams%}" rel="facebook" class="icon facebook" title="Facebook"></a></li>
                                                        <li><a target="_blank" href="https://twitter.com/intent/tweet?{%=o.twitterShareParams%}" rel="twitter" class="icon twitter" title="Twitter"></a></li>
                                                        <li><a target="_blank" href="https://plus.google.com/share?{%=o.googleplusShareParams%}" rel="googleplus" class="icon googleplus" title="Google+"></a></li>
                                                        <li class="clr"></li>
                                                    </ul>
                                                </div>
                                            </div>
                                            &gt;script>dropdownPanels.init();&gt;/script>
                                        </div>
                                        <div class="col-3">
                                            <a href="javascript:void(0)" class="close" onclick="unitManagerMap.hideDetails()">Back</a>
                                        </div>
                                        <div class="clr"></div>
                                    </div>
                                </div>
                                <div id="partyManagerContactForm">
                                    <div class="bottomPanel">
                                        <div class="cols">

                                            
                                            
                                            <form class="form-framework" action="{%=o.submitURL%}" method="post" data-send-method="ajax" data-type="html" data-callback="unitManagerMap.onResponse" onsubmit="updateGDPRfields.init(this, '#gdpr')">
                                                <input type="hidden" name="{%=o.referrerParamName%}" value="{%=o.referrerParamValue%}" />
                                                <input type="hidden" name="managerIdentifier" value="{%=o.id%}" />
                                                <input type="hidden" name="dealerFirstName" value="{%=o.firstName%}" />
                                                <input type="hidden" name="isSubmitted" value="true" />
                                                <input type="hidden" name="formGuid" value="" data-type="form-guid" data-value="/service/tupperware-au/appng-webutils/webservice/getFormGuid" />
                                                <div class="contactData">
                                                    <div class="managerData">
                                                        Contact {%=o.positionName%}: <br/>
                                                        <span class="name">{%=o.firstName%} {%=o.lastName%}</span>
                                                    </div>
                                                    {% if (o.applicationMode == 'machparty') { %}
                                                    <div class="row">
                                                        <label for="gender">Title</label>
                                                        <select data-type="select" name="gender" id="gender" data-error-message-selector="#gender_error">
                                                            <option value="">select ...</option>
                                                            <option value="F">Ms.</option>
                                                            <option value="M">Mr.</option>
                                                        </select><span id="gender_error" class="error-message"></span>
                                                    </div>
                                                    {% } %}
                                                    
                                                    
                                                    <div class="row">
                                                        <label for="firstName">First Name *</label><input data-type="text" required type="text" name="firstName" id="firstName" value="" data-error-message-selector="#firstName_error" data-error-message="Required"><span id="firstName_error" class="error-message"></span>
                                                    </div>
                                                    
                                                    <div class="row">
                                                        <label for="lastName">Last Name *</label><input data-type="text" required type="text" name="lastName" id="lastName" value="" data-error-message-selector="#lastName_error" data-error-message="Required"><span id="lastName_error" class="error-message"></span>
                                                    </div>
                                                    
                                                    <div class="row">
                                                        <label for="phone">Phone </label><input data-type="text"  type="text" name="phone" id="phone" value="" data-reg-exp="^[+]*[0-9\s]*[(]*[0-9\s]*[)]*[\-0-9\s]*$" data-error-message-selector="#phone_error" data-error-message="Required" data-error-type-message=""><span id="phone_error" class="error-message"></span>
                                                    </div>
                                                    <div class="row">
                                                        <label for="email">E-mail *</label><input data-type="email" required type="text" name="email" id="email" value="" data-error-message-selector="#email_error" data-error-message="Required" data-error-type-message="Data type error"><span id="email_error" class="error-message"></span>
                                                    </div>
    												                                                      
                                                    <div class="row">
                                                        <label for="street">Address </label><input data-type="text"  type="text" name="street" id="street" value="" data-error-message-selector="#street_error" data-error-message="Required"><span id="street_error" class="error-message"></span>
                                                    </div>
                                                                                                        
                                                    <div class="row">
                                                        <label for="zip">Postcode </label><input data-type="text"  type="text" name="zip" id="zip" value="" data-error-message-selector="#zip_error" data-error-message="Required"><span id="zip_error" class="error-message"></span>
                                                    </div>
                                                    <div class="row">
                                                        <label for="state">Country *</label>
                                                        <select data-type="select" name="countryName" required id="state" data-error-message-selector="#state_error" data-error-message="Required">
                                                                <option value="">select ...</option>
                                                                
                                                                <option value="AU">AU</option>
                                                                <option value="NZ">NZ</option>
                                                        </select><span id="state_error" class="error-message"></span>
                                                    </div>
                                                    
                                                    <input type="hidden" name="city" value="" />
                                                    
                                                    <div class="row">
                                                        <label for="subject">How can I help you *</label>
                                                        <select data-type="select" name="subject" required id="subject" data-error-message-selector="#subject_error" data-error-message="Required">
                                                                <option value="">select ...</option>
                                                                <option value="buy">Make a purchase</option>
                                                                <option value="party">Book a party</option>
                                                                <option value="join">Become a consultant</option>
                                                                
                                                                
                                                                <option value="other">Other</option>
                                                        </select><span id="subject_error" class="error-message"></span>
                                                    </div>
                                                    <div class="row">
                                                        <label for="comments" class="forTextarea">Your message </label><textarea data-type="textarea"  name="comments" id="comments" data-error-message-selector="#comments_error" data-error-message="Required"></textarea><span id="comments_error" class="error-message"></span>
                                                        <div class="clr"></div>
                                                    </div>
                                                    <div class="row">
                                                        <label for="newsletter_registration" class="forCheckbox"><span><input type="checkbox" data-type="checkbox" value="yes" id="newsletter_registration" name="newsletter_registration"> <p>I agree that Tupperware Australia &amp; New Zealand collects my stated contact details to inform me by email about interesting services, products and special promotions with respect to Tupperware. The delivery of the newsletter works according to our&nbsp;<a href="/privacy-policy">Privacy Policy</a>. I can withdraw my consent at Tupperware at any time via clicking on the unsubscribe link at the bottom of every newsletter.</p> </span></label>
                                                        <div class="clr"></div>
                                                    </div>
                                                    
                                                    <input type="hidden" id="gdpr_date" name="gdprdate" value="" />
													<input type="hidden" id="gdpr_text" name="gdprtext" value="" />
													
                                                    
                                                    
                                                </div>
                                                <div class="row buttons">
                                                    
                                                    <input type="submit" name="submitBT" value="Send">
                                                    <input type="reset" name="resetBT" value="Cancel" onclick="unitManagerMap.hideContactForm()">
                                                </div>
                                                <div class="row">
                                                    <div class="global-error-message"></div>
                                                </div>
                                                <div class="requiredLegend">
                                                    *  Required field

                                                </div>
                                            </form>
                                            &gt;script>
                                                forms.init();
                                                app.uncryptMailto();
                                            &gt;/script>
                                            <a href="javascript:void(0)" class="close" onclick="unitManagerMap.hideContactForm()"><span>X</span></a>
                                        </div>
                                    </div>
                                </div>
                                <div id="partyManagerContactData">
                                    <div class="bottomPanel">
                                        <div class="cols">
                                            <div class="col col-1-4">
                                                <div class="contactData">
                                                    <span class="name">{%=o.firstName%} {%=o.lastName%}</span>
                                                    <p>
                                                        {%=o.positionName%}<br/>
                                                        {%=o.street%}<br/>
                                                        {%=o.zipcode%} {%=o.city%}
                                                    </p>
                                                    <div class="slider closed">
                                                        <a href="javascript:void(0)" class="button" onclick="slider.toggle(this);ga('send', 'event', 'Connect DL profile page', 'Click on button', '{%=o.labelContactMe%}')"><span>{%=o.labelContactMe%}</span></a>
                                                        <div class="content">
                                                            <ul>
                                                                <li>
                                                                    <a href="javascript:void(0)" class="icon-button mail" onclick="unitManagerMap.showContactForm();ga('send', 'event', 'Connect DL profile page', 'Click on button', 'E-Mail')"><span class="hide">mail</span></a>
                                                                </li>
                                                                {% print(o.contactItems, true); %}
                                                            </ul>
                                                            <div class="item" data-rel="telefon">
                                                                {% print(o.phone, true); %}
                                                            </div>
                                                            <div class="item" data-rel="mobil">
                                                                {% print(o.mobile, true); %}
                                                            </div>
                                                            <div class="item" data-rel="whatsapp">
                                                                {% print(o.whatsapp, true); %}
                                                            </div>
                                                        </div>
                                                    </div>
													{% if (o.shopUrl) { %}
													<div class="slider mt-05">
														<a href="{%=o.shopUrl%}" target="_blank" class="button"><span>{%=o.labelShopNow%}</span></a>
													</div>
													{% } %}
                                                    <p class="for-full-screen-map-only">
                                                        <a href="javascript:void(0)" class="button icon find-me" onclick="unitManagerMap.showMapLayer('#detailsGMap');"><span>{%=o.labelFindMe%}</span></a>
                                                    </p>
                                                </div>
                                                {% print(o.paymentData, true); %}
                                                {% print(o.socialNetworks, true); %}
                                                {% print(o.onlineParty, true); %}
                                            </div>
                                            <div class="col {%=o.mapContainerClass%}">
                                                <div class="detailsGMap googleMaps" id="detailsGMap">
                                                </div>
                                            </div>
                                            {% if (o.extraDataMode == 'languages-only') { %}
                                                {% print(o.languagesData, true); %}
                                            {% } %}
                                            {% if (o.extraDataMode == 'specialRecipeList-only') { %}
                                                {% print(o.specialistInRecipes, true); %}
                                            {% } %}
                                            {% if (o.extraDataMode == 'languages-above') { %}
                                                {% print(o.languagesData, true); %}
                                                {% print(o.specialistInRecipes, true); %}
                                            {% } %}
                                            {% if ( o.extraDataMode == '' || o.extraDataMode == 'all' ) { %}
                                                {% print(o.specialistInRecipes, true); %}
                                                {% print(o.languagesData, true); %}
                                            {% } %}
                                            <div class="clr"></div>
                                            <a onclick="unitManagerMap.hideDetails()" class="close" href="javascript:void(0)"><span>X</span></a>
                                        </div>
                                        <p class="disclaimer" id="disclaimer">
                                        {% if (o.applicationMode == 'machparty') { %}
                                            {% if (o.type != "DS") { %}
                                            DS MachParty profile <span data-rel="distributor-link">{% print(unitManagerMap.getDistributorLink(o.userName), true); %}</span>
                                            {% } %}
                                        {% } else { %}
                                             <span></span>
                                        {% } %}
                                        </p>
                                    </div>
                                </div>
                            </div>                            
                        
                        </script>
                        
                        <script type="text/x-tmpl" id="confirmationMessage">
                            <div class="row">
                                <div class="confirmation">
                                    {% print(o.message, true); %}
                                    <div class="buttons">
                                        <a href="/catalogues/promotions">Special Offers</a>
                                        <a href="javascript:void(0)" onclick="unitManagerMap.hideContactForm()">Back</a>
                                    </div>
                                </div>
                            </div>
                        </script>
                        
                        <script type="text/x-tmpl" id="customCommunicationLoader">
                            <div class="communicationLoader" id="communicationLoader">
                                <div>
                                    <div class="wrapper">
                                        <span>Loading ... Please wait</span>
                                    </div>
                                </div>
                            </div>
                        </script>
                        

                        <section class="row partyManager" id="partyManagerSection">
                            <div class="sectionWrap">
                                <h1 class="ultralight">Select your consultant</h1>
                                <div id="partyContact" class="partyContactWrap jcarousel-wrapper">
                                    <a href="javascript:void(0)" class="prevButton jcarousel-control-prev">&nbsp;</a>
                                    <div class="jcarousel">
                                        <ul class="partyContactSelector" id="partyContactSelector">
                                        </ul>
                                    </div>
                                    <a href="javascript:void(0)" class="nextButton jcarousel-control-next">&nbsp;</a>
                                </div>
                            </div>
                        </section>
                        <section class="row partyManagerMap" id="partyManagerMap">
                            <div class="contentRow">
                                <div id="gmap" class="googleMaps"></div>
                            </div>
                            <div class="foot green animated" data-type="down">
                                <span class="prefix label">Search for a consultant</span>
                                <form onsubmit="unitManagerMap.searchAddress(this.address.value,true);return false;" class="content_form gmapSearch inline" name="formular-map" method="get" action="javascript:void(0)">
                                    <input type="text" placeholder="Suburb or city" value="" name="address" data-type="address" />
                                    <input type="submit" name="submitBT" value="Go" />
                                </form>
                            </div>
                        </section>

                        
                        
                        <script type="text/javascript">

                            setTimeout(function(){
                                var jsTag = document.createElement('script');
                                jsTag.type = "text/javascript";
                                jsTag.src = "//maps.google.com/maps/api/js?key=AIzaSyBrnznCzBmVtCpgx2-wGs_1y7aXVNRni6s&sensor=false&language=en&libraries=places";
                                document.getElementsByTagName('body')[0].appendChild(jsTag);    
                            }, 100);

                                                
                            beforeInit.push(function(){
                            
                                if (isTouchDevice && (app.getWindowWidth()<=600)) {
                                    $('#gmap').height(app.getContentAreaHeight()+'px');
                                }
                            });
                            
                            afterInit.push(function(){
                            
                                unitManagerMap.buildLoader('gmap','/assets/system_images/preload.gif');
                                unitManagerMap.init({
                                
                                    serverAppURL: '/service/connect/connect/webservice/locateDealerService',
                                    applicationMode: '',
                                    extraDataMode: 'all', //[empty = all|languages-only|specialRecipeList-only|languages-above]
                                    getDetailsByIdURL: '/service/connect/connect/webservice/locateDealerService',
                                    contactFormActionURL: '/en/submit',
                                    loadingIconURL: '/assets/system_images/preload.gif',
                                    countryName: 'Australia',
                                    countryNameENG: 'Australia;New Zealand',
                                    typesURLParam: '&types=political',
                                    countryCode: 'AU',
                                    languageCode: 'en',
                                    webserviceItemId: 'AU',
                                    showCountryNameInInfoWindowAndTable: false,
                                    showWhatsAppData: true,
                                    isFormAddress: true,
                                    clustered: true,
                                    openClusterInfoWindowTwice: false,
                                    searchOnlyByDefinedCountry: false,
                                    sortItemsInTableBy:  'distance',
                                    mapWithPOI: false,
                                    defaultPosition: {
                                        lat: -29.38217507514, 
                                        lng: 149.765625
                                    },
                                    mapZoom: {
                                        min: 4,
                                        max: 21,
                                        _default: 4,
                                        details: 11
                                    },
                                    restrictionConf: {
                                        minItemsFound: 2

                                    },
                                    label: {
                                        distributorship: "Distributorship",
                                        unitManager: "Unit Manager",
                                        teamLeader: "Team Leader",
                                        dealer: "Consultant",
                                        infoCenter: "Information Center",
                                        link: "Connect!",
                                        region: "Region",
                                        phone: "Phone",
                                        fax: "Fax",
                                        mobile: "Mobile",
                                        email: "E-mail",
                                        locationFound: "Location found",
                                        paymentInfo: "Payment Info",
                                        socialNetworks: "Social Media",
                                        specialistInRecipes: "My specialties",
                                        whatsapp: "WhatsApp",
                                        share: "Share",
                                        copyTip: "Copyright tip",
                                        connectLinkTitle: "Connect!",
                                        exploreYourArea: "Search for a consultant",
                                        contactMe: "Contact me",
                                        findMe: "Find me",
                                        disclaimer: "",
                                        languages: "Languages",
                                        shopNow: "Shop now",
                                        onlineParty: ""
                                    },
                                    message: {
                                        0: "The position cannot be found."
                                    },
                                    iconType: {
                                        //Distributorship
                                        0: {
                                            url: '/assets/system_images/distributor-marker-v2.png',
                                            shadow: null,
                                            width: 59,
                                            height: 74,
                                            anchorPosition: {
                                                x: parseInt(59 / 2),
                                                y: parseInt(74 / 2)
                                            },
                                            infoWindowPosition: {
                                                x: parseInt(59 / 2),
                                                y: 0
                                            },
                                            imageMap: new Array(0,0, 0,59, 59,74, 74,0)
                                        },
                                        //Unit Manager
                                        1: {
                                            url: '/assets/system_images/manager-marker-v2.png',
                                            shadow: null,
                                            width: 59,
                                            height: 74,
                                            anchorPosition: {
                                                x: parseInt(59 / 2),
                                                y: parseInt(74 / 2)
                                            },
                                            infoWindowPosition: {
                                                x: parseInt(59 / 2),
                                                y: 0
                                            },
                                            imageMap: new Array(0,0, 0,59, 59,74, 74,0)
                                        },
                                        //Team Leader
                                        2: {
                                            url: '/assets/system_images/manager-marker-v2.png',
                                            shadow: null,
                                            width: 59,
                                            height: 74,
                                            anchorPosition: {
                                                x: parseInt(59 / 2),
                                                y: parseInt(74 / 2)
                                            },
                                            infoWindowPosition: {
                                                x: parseInt(59 / 2),
                                                y: 0
                                            },
                                            imageMap: new Array(0,0, 0,59, 59,74, 74,0)
                                        },
                                        //Dealer
                                        3: {
                                            url: '/assets/system_images/manager-marker-v2.png',
                                            shadow: null,
                                            width: 59,
                                            height: 74,
                                            anchorPosition: {
                                                x: parseInt(59 / 2),
                                                y: parseInt(74 / 2)
                                            },
                                            infoWindowPosition: {
                                                x: parseInt(59 / 2),
                                                y: 0
                                            },
                                            imageMap: new Array(0,0, 0,59, 59,74, 74,0)
                                        },
                                        //Info Center
                                        4: {
                                            url: '/assets/system_images/manager-marker-v2.png',
                                            shadow: null,
                                            width: 59,
                                            height: 74,
                                            anchorPosition: {
                                                x: parseInt(59 / 2),
                                                y: parseInt(74 / 2)
                                            },
                                            infoWindowPosition: {
                                                x: parseInt(59 / 2),
                                                y: 0
                                            },
                                            imageMap: new Array(0,0, 0,59, 59,74, 74,0)
                                        }        
                                    },
                                    
                                    clusterStyles: [[
                                        {
                                            url: '/assets/system_images/icon-gmap-cluster-marker.png',
                                            height: 74, 
                                            width: 59,
                                            anchor: [54, 0],
                                            textColor: '#ffffff',
                                            textSize: 16
                                        }
                                    ]],
                                    afterInit: function(){
                                        app.scrollToRequestedSection()
                                    },
                                    urlParameterNumber4DealerID: 2,
                                    defaultPersonPicture: '/assets/system_images/dummy-person-picture.gif',
                                    shuffleCarouselItemsEnabled: false,
                                    autocompleteSearch: true,
                                    hideZipcodeInInfoTable: false

                                });                                
                            });
                                                
                        </script>
                        
                        <!-- -->


                        <section class="row" id="themes">
                            <div class="sectionWrap">
                                <h1 class="ultralight">Party recipe inspiration</h1>
                                <div class="themesWrap">                        
                                    <div id="recipeSlider" class="themesSelector">
                                        

                                       <div id="fr2_1292" class="slide">
                                            <div class="theme">
                                                <a href="https://www.tupperware.com.au/recipes/recipe/9268/PulledBeefBurgers?fid=D54609852C0E4AB297789CFBCB51E7ECENA" target="_blank">
                                                <figure class="image">
                                                    <img class="lazy-scrolling" data-width="728" data-height="538" data-src="/assets/images/mediacenter-images/063921-Pulled-Beef-Burgers.jpg" alt="Speedy Pulled Beef Burgers" />
                                                    <div class="title"><h3>Speedy Pulled Beef Burgers</h3></div>
                                                </figure>
                                            </a>
                                            </div>
                                        </div>
 

                                       <div id="fr2_1293" class="slide">
                                            <div class="theme">
                                                <a href="https://www.tupperware.com.au/recipes/recipe/8547/IcedChampagneCocktail?fid=D54609852C0E4AB297789CFBCB51E7ECENA" target="_blank">
                                                <figure class="image">
                                                    <img class="lazy-scrolling" data-width="728" data-height="538" data-src="/assets/images/mediacenter-images/061190-Iced-Cocktail-Champagne.jpg" alt="Happy Hour" />
                                                    <div class="title"><h3>Happy Hour</h3></div>
                                                </figure>
                                            </a>
                                            </div>
                                        </div>
 

                                       <div id="fr2_1294" class="slide">
                                            <div class="theme">
                                                <a href="https://www.tupperware.com.au/recipes/recipe/7755/QuickChocolateCake?fid=D54609852C0E4AB297789CFBCB51E7ECENA" target="_blank">
                                                <figure class="image">
                                                    <img class="lazy-scrolling" data-width="728" data-height="538" data-src="/assets/images/mediacenter-images/061199-Quick-Chocolate-Cake.jpg" alt="Chocoholics Party" />
                                                    <div class="title"><h3>Chocoholics Party</h3></div>
                                                </figure>
                                            </a>
                                            </div>
                                        </div>
 
                                    </div>
                                    <div class="prevButtonContainer"><a href="javascript:void(0)" class="prevButton" id="recipeSliderPrev">Prev</a></div>
                                    <div class="nextButtonContainer"><a href="javascript:void(0)" class="nextButton" id="recipeSliderNext">Next</a></div>
                                </div>                                    
                            </div>
                        </section> 
 

                        <section class="row"></section>
                    </div>
                    
                    <footer>
                        <section class="row">
                            <div class="contentPanel footer">
                                <div class="contentRow">
                                    
                                    <div class="logo"><img class="lazy-scrolling" data-width="359" data-height="58" data-src="/assets/system_images/tupperware-connect-logo.png" /></div>
                                    <div class="row threeColumn">
                                        <!-- -->
                                        <div class="col">
                                            <div class="colTeaser">

                                                <a target="_blank" href="https://www.tupperware.com.au/catalogues">
                                                    <span class="picture"><img alt="" class="lazy-scrolling" data-width="140" data-height="140" data-src="/assets/images/catalog-book.png" /></span>
                                                    <p>Catalogue</p>                                                            
                                                </a>
                                            </div>
                                        </div><!-- -->
                                        <div class="col">
                                            <div class="colTeaser">

                                                <a target="_blank" href="https://www.tupperware.com.au/catalogues/promotions">
                                                    <span class="picture"><img alt="" class="lazy-scrolling" data-width="140" data-height="140" data-src="/assets/images/burst.png" /></span>
                                                    <p>Special Offers</p>                                                            
                                                </a>
                                            </div>
                                        </div><!-- -->
                                        <div class="col">
                                            <div class="colTeaser">

                                                <a target="_blank" href="https://www.tupperware.com.au/newsletter">
                                                    <span class="picture"><img alt="" class="lazy-scrolling" data-width="140" data-height="140" data-src="/assets/images/letter.png" /></span>
                                                    <p>Newsletter</p>                                                            
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="clr"></div>
                                </div>
                                
                                <div class="contentRow">
                                    <div class="socialMedia col">
                                        <ul>
                                            <li><a target="_blank" class="instagram" href="https://www.instagram.com/tupperwareausnz"><span>Instagram</span></a></li>
<li><a target="_blank" class="facebook" href="https://www.facebook.com/tupperwareausnz"><span>Facebook</span></a></li>
<li><a target="_blank" class="youtube" href="https://www.youtube.com/user/TupperwareAUSNZ"><span>YouTube</span></a></li>
<li><a target="_blank" class="pinterest" href="https://www.pinterest.com.au/tupperwareausnz/"><span>Pinterest</span></a></li>

                                        </ul>
                                        
                                        <ul>
                                            
                                        </ul>
                                        
                                    </div>
                                    <div class="clr"></div>
                                </div>
                            
                                <div class="contentPanel navigation">
                                    <div class="contentRow">
                                        <nav class="col">
                                            <ul>
                                                <li><a target="_blank" href="/index"></a></li>
                                                <li><a target="_blank" href="/delivery">Delivery</a></li><li><a target="_blank" href="https://sustainability.tupperwarebrands.com">Sustainability</a></li><li><a target="_blank" href="/contact">Contact Us</a></li><li><a target="_blank" href="/about-us">About Us</a></li><li><a target="_blank" href="/our-plastics">Our Plastics</a></li><li><a target="_blank" href="/terms-and-conditions">Terms and Conditions</a></li><li><a target="_blank" href="https://www.tupperwarebrands.com/">Tupperware Brands</a></li><li><a target="_blank" href="/privacy-policy">Privacy Policy</a></li><li><a target="_blank" href="/terms-of-use">Terms of Use</a></li><li><a target="_blank" href="/newsletter">Newsletter</a></li><li><a target="_blank" href="/tupperware-worldwide">Tupperware Worldwide</a></li>
                                                
                                                <li>&copy; Tupperware 2020</li>
                                            </ul>
                                        </nav>
                                        <div class="clr"></div>
                                    </div>                            
                                </div>
                                
                            </div>
                        </section>
                    </footer>
                    <script type="text/javascript">
                                            
                                            
                            afterInit.push(function(){
                                
                                var getSliderHeight = function(oImg) {
                                
									var ration = $(oImg).attr('data-width')/$(oImg).attr('data-height');
									var winWidth = app.getWindowWidth();
									
									if (winWidth<=800) {
										return (winWidth/ration);
									} else {
										return 568;
									}
                                
                                }
                                
                                $.imagesPreloader($('#recipeSlider img').get(), function(){
                                    
                                    $('#recipeSlider').css({
                                        'width': 'auto',
                                        'maxWidth': '1900px'
                                    });

                                    //recipeSlider
                                    var recipeSlider = new FilmRoll({
                                        scroll: false,
                                        pager: false,
                                        container: '#recipeSlider',
                                        height: (getSliderHeight($('#recipeSlider img').get(0))+'px'),
                                        prev: '#recipeSliderPrev',
                                        next: '#recipeSliderNext'
                                    });
                                    
                                    $(window).trigger('resize');
                                });
                                
                                
                                
                            });
                            
                    </script>
                                    
                    <ul class="fixedBottomControlPanel">
                        <li class="right"><a class="icon arrowTop" onclick="app.scrollTo(0)" href="javascript:void(0)">top</a></li>
                        <li class="clr"></li>
                    </ul>                                    
                    
                </div>
            </div><!-- cover -->
            
        </div>
        
        <!-- CSS -->
        
        

        <script type="text/javascript">        
            function onloadCSS(e,n){e.onload=function(){e.onload=null,n&&n.call(e)},"isApplicationInstalled"in navigator&&"onloadcssdefined"in e&&e.onloadcssdefined(n)}!function(e){"use strict";var n=function(n,t,o){function i(e){if(a.body)return e();setTimeout(function(){i(e)})}function d(){r.addEventListener&&r.removeEventListener("load",d),r.media=o||"all"}var l,a=e.document,r=a.createElement("link");if(t)l=t;else{var s=(a.body||a.getElementsByTagName("head")[0]).childNodes;l=s[s.length-1]}var f=a.styleSheets;r.rel="stylesheet",r.href=n,r.media="only x",i(function(){l.parentNode.insertBefore(r,t?l:l.nextSibling)});var u=function(e){for(var n=r.href,t=f.length;t--;)if(f[t].href===n)return e();setTimeout(function(){u(e)})};return r.addEventListener&&r.addEventListener("load",d),r.onloadcssdefined=u,u(d),r};"undefined"!=typeof exports?exports.loadCSS=n:e.loadCSS=n}("undefined"!=typeof global?global:this);                
            var ss = loadCSS("/en/connect-screen.min.css?ts=v1_02/17/2020 14:50_8");
            onloadCSS( ss, function() {
                //load JS
                var jsTag = document.createElement('script');
                jsTag.type = "text/javascript";
                jsTag.src = "/en/connect-script.min.js?ts=v1_02/17/2020 14:50_8";
                document.getElementsByTagName('body')[0].appendChild(jsTag);
            });    
        </script>


        
    </body>

</html><!-- false 0 0 connect  -->