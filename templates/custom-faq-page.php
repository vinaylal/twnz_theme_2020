<?php
/*
 * Template name: FAQ Page
 * @link https://developer.wordpress.org/themes/basics/template-hierarchy/
 *
 * @package tupperware.co.nz
 */

?>

<?php get_header(); ?>
                    
<div id="content">
    <section class="row">
        <div id="leftColumn" class="col">
            <div class="wrapper">
                <div id="leftNavigation">
                    
                    <?php get_template_part( 'template-parts/categories-menu' ); ?>

                </div>
                
            </div>
        </div>
    
        <div id="middleColumn" class="col">
            <div class="wrapper">

                <?php 
                    if ( have_posts() ) {
                        while ( have_posts() ) {
                            the_title( '<h1 class="entry-title">', '</h1>' );
                            the_post();
                            the_content();
                        } // end while
                    } // end if
                ?>

                <?php
                if( have_rows('faqs') ):
                    while( have_rows('faqs') ) : the_row();
                        ?>
                        <div class="contentRow dottedBottom">
                            <div class="text">
                                <h2 style="margin-bottom:0.0001pt; margin-left:0cm; margin-right:0cm; margin-top:0cm"><strong><?php echo get_sub_field('title'); ?></strong>
                                </h2>
                            </div>
                            <div class="clr"></div>
                        </div>
                        <div class="contentRow dottedBottom">  
                            <div class="nAccordian">
                                <ul class="faqQuestions">
                                    <?php
                                        // Loop over sub repeater rows.
                                        if( have_rows('qapair') ):
                                            while( have_rows('qapair') ) : the_row();
                                    ?>
                                                <li>
                                                    <a class="question">
                                                        <span>
                                                            <?php echo get_sub_field('question'); ?>
                                                        </span>
                                                    </a>
                                                    <div class="answer contentRow dottedBottom">
                                                        
                                                        <div class="text">
                                                            <div class="p">  
                                                                <?php echo get_sub_field('answer'); ?>                                                 
                                                            </div>
                                                        </div>
                                                        <div class="clr"></div>
                                                    </div>
                                                </li>
                                    <?php
                                            endwhile;
                                        endif;
                                    ?>
                                </ul>
                            </div>
                        </div>
                    <?php endwhile;
                endif;?>             
                        </div>        
            </div>
        </div>              
    </section>
</div>

<?php get_footer(); ?>