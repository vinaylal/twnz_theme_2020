<?php

/*
 * Template Name: Find Brands Page
 * description: >-
 */

?>

<?php get_header(); ?>

<div id="content">
    <section class="row">
    
        <div id="leftColumn" class="col">
            <div class="wrapper">
                <div id="leftNavigation">
                    <?php get_template_part( 'template-parts/categories-menu' ); ?>
                </div>
            </div>
        </div>
    
        <div id="middleColumn" class="col">
            <div class="wrapper">

                <?php 
                    if ( have_posts() ) {
                        while ( have_posts() ) {
                            the_title( '<h1 class="entry-title">', '</h1>' );
                        }
                    }
                ?>   

                <div class="contentRow  dottedBottom">
                    <div class="clr"></div>
                </div>

                <div class="contentRow">
                    <div id="worldwideMap">
                        <p>Loading ...</p>
                    </div>
                    <p></p>
                </div>
                
                <div class="contentRow">
                    <form id="worldwideList" action="javascript:void(0)">

                        <fieldset>
                            <div class="row">
                                <div class="col">
                                    <label for="address">Location:</label>
                                    <select name="worldwideDropDown">
                                        <option value="">Please select ...</option>
                                        <option value="NA">North America</option>
                                        <option value="SA">South America</option>
                                        <option value="EU">Europe</option>
                                        <option value="AF">Africa</option>
                                        <option value="AP">Asia &amp; Pacific</option>
                                    </select>
                                </div>
                                <div class="col">
                                </div>
                            </div>
                            <div class="row"></div>
                        </fieldset>
                        
                    </form>                                        
                </div>
                
                <div class="contentRow dottedBottom">                                        
                    <div id="worldwideListing">
                        <div class="content" rel="NA">
                            <h2>North America</h2>
                            
                            <div class="grayPanel">
                                <ul class="rowAnchor">
                                    <li class="first dottedBottom"><div class="name"><b>Country</b></div><div class="address"><b>Contact</b></div><div class="clr"></div></li>
                                    <li><div class="name"><a href="http://www.tupperware.ca/" target="_blank" class="block">Canada</a></div><div class="address"><a href="http://www.tupperware.ca/" target="_blank" class="block">www.tupperware.ca</a></div><div class="clr"></div></li>
                                    <li><div class="name"><a href="javascript:void(0)" class="block">Costa Rica</a></div><div class="address"><a href="javascript:void(0)" class="block">506-224-4000</a></div><div class="clr"></div></li>
                                    <li><div class="name"><a href="javascript:void(0)" class="block">El Salvador</a></div><div class="address"><a href="javascript:void(0)" class="block">011-503-2242459</a></div><div class="clr"></div></li>
                                    <li><div class="name"><a href="javascript:void(0)" class="block">Guatemala</a></div><div class="address"><a href="javascript:void(0)" class="block">502-485-7652</a></div><div class="clr"></div></li>
                                    <li><div class="name"><a href="https://www.tupperware.com.mx/" target="_blank" class="block">Mexico</a></div><div class="address"><a href="https://www.tupperware.com.mx/" target="_blank" class="block">www.tupperware.com.mx</a></div><div class="clr"></div></li>
                                    <li><div class="name"><a href="javascript:void(0)" class="block">Puerto Rico</a></div><div class="address"><a href="javascript:void(0)" class="block">787-798-4444</a></div><div class="clr"></div></li>
                                    <li><div class="name"><a href="http://www.tupperware.com" target="_blank" class="block">United States</a></div><div class="address"><a href="http://www.tupperware.com" target="_blank" class="block">www.tupperware.com</a></div><div class="clr"></div></li>
                                </ul>
                                <ul class="rowAnchor">
                                    <li class="first dottedBottom"><div class="name"><b>Other Brands</b></div><div class="address"><b>Contact</b></div><div class="clr"></div></li>
                                    <li><div class="name"><a href="https://www.fuller.com.mx" target="_blank" class="block">Fuller (Mexico)</a></div><div class="address"><a href="https://www.fuller.com.mx" target="_blank" class="block">www.fuller.com.mx</a></div><div class="clr"></div></li>
                                </ul>
                            </div>
                        
                        </div>
                        
                        <div class="content" rel="SA">
                            <h2>South America</h2>
                            <div class="grayPanel">
                                <ul class="rowAnchor">
                                    <li class="first dottedBottom"><div class="name"><b>Country</b></div><div class="address"><b>Contact</b></div><div class="clr"></div></li>
                                    <li><div class="name"><a href="https://www.tupperware.com.ar/" target="_blank" class="block">Argentina</a></div><div class="address"><a href="https://www.tupperware.com.ar/" target="_blank" class="block">www.tupperware.com.ar</a></div><div class="clr"></div></li>
                                    <li><div class="name"><a href="javascript:void(0)" class="block">Bolivia</a></div><div class="address"><a href="javascript:void(0)" class="block">(11) 4313-1033</a></div><div class="clr"></div></li>
                                    <li><div class="name"><a href="https://www.tupperware.com.br/" target="_blank" class="block">Brazil</a></div><div class="address"><a href="https://www.tupperware.com.br/" target="_blank"  class="block">www.tupperware.com.br</a></div><div class="clr"></div></li>
                                    <li><div class="name"><a href="https://www.tupperware.com.co/" target="_blank" class="block">Colombia</a></div><div class="address"><a href="https://www.tupperware.com.co/" target="_blank" class="block">www.tupperware.com.co</a></div><div class="clr"></div></li>
                                    <li><div class="name"><a href="javascript:void(0)" class="block">Costa Rica</a></div><div class="address"><a href="javascript:void(0)" class="block">506-224-4000</a></div><div class="clr"></div></li>
                                    <li><div class="name"><a href="https://www.tupperware.com.ec/" target="_blank" class="block">Ecuador</a></div><div class="address"><a href="https://www.tupperware.com.ec/" target="_blank" class="block">www.tupperware.com.ec</a></div><div class="clr"></div></li>
                                    <li><div class="name"><a href="javascript:void(0)" class="block">El Salvador</a></div><div class="address"><a href="javascript:void(0)" class="block">011-503-2242459</a></div><div class="clr"></div></li>
                                    <li><div class="name"><a href="javascript:void(0)" class="block">Guatemala</a></div><div class="address"><a href="javascript:void(0)" class="block">502-485-7652</a></div><div class="clr"></div></li>
                                    <li><div class="name"><a href="javascript:void(0)" class="block">Paraguay</a></div><div class="address"><a href="javascript:void(0)" class="block">(11) 4313-1033</a></div><div class="clr"></div></li>
                                    <li><div class="name"><a href="javascript:void(0)" class="block">Peru</a></div><div class="address"><a href="javascript:void(0)" class="block">58-212-992-2922</a></div><div class="clr"></div></li>
                                    <li><div class="name"><a href="javascript:void(0)" class="block">Puerto Rico</a></div><div class="address"><a href="javascript:void(0)" class="block">787-798-4444</a></div><div class="clr"></div></li>
                                    <li><div class="name"><a href="https://www.tupperware.com.uy/" target="_blank" class="block">Uruguay</a></div><div class="address"><a href="https://www.tupperware.com.uy/" target="_blank"  class="block">www.tupperware.com.uy</a></div><div class="clr"></div></li>
                                    <li><div class="name"><a href="https://www.tupperware.com.ve/" target="_blank" class="block">Venezuela</a></div><div class="address"><a href="https://www.tupperware.com.ve/" target="_blank" class="block">www.tupperware.com.ve</a></div><div class="clr"></div></li>
                                </ul>
                                <ul class="rowAnchor">
                                    <li class="first dottedBottom"><div class="name"><b>Other Brands</b></div><div class="address"><b>Contact</b></div><div class="clr"></div></li>
                                    <li><div class="name"><a href="http://www.nuvo.com.uy/" target="_blank" class="block">Nuvo (Uruguay)</a></div><div class="address"><a href="http://www.nuvo.com.uy/" target="_blank" class="block">www.nuvo.com.uy</a></div><div class="clr"></div></li>
                                </ul>
                            </div>
                        </div>
                        
                        <div class="content" rel="EU">
                            <h2>Europe</h2>
                            <div class="grayPanel">
                                <ul class="rowAnchor">
                                    <li class="first dottedBottom"><div class="name"><b>Country</b></div><div class="address"><b>Contact</b></div><div class="clr"></div></li>
                                    <li><div class="name"><a href="https://www.tupperware.at/" target="_blank" class="block">Austria</a></div><div class="address"><a href="https://www.tupperware.at/" target="_blank" class="block">www.tupperware.at</a></div><div class="clr"></div></li>
                                    <li><div class="name"><a href="https://www.tupperware.be/" target="_blank" class="block">Belgium</a></div><div class="address"><a href="https://www.tupperware.be/" target="_blank" class="block">www.tupperware.be</a></div><div class="clr"></div></li>
                                    <li><div class="name"><a href="https://www.tupperware.ba/" target="_blank" class="block">Bosnia and Herzegovina</a></div><div class="address"><a href="https://www.tupperware.ba/" target="_blank" class="block">www.tupperware.ba</a></div><div class="clr"></div></li>
                                    <li><div class="name"><a href="https://www.tupperware.bg/" target="_blank" class="block">Bulgaria</a></div><div class="address"><a href="https://www.tupperware.bg/" target="_blank" class="block">www.tupperware.bg</a></div><div class="clr"></div></li>
                                    <li><div class="name"><a href="https://www.tupperware.hr/" target="_blank" class="block">Croatia</a></div><div class="address"><a href="https://www.tupperware.hr/" target="_blank" class="block">www.tupperware.hr</a></div><div class="clr"></div></li>
                                    <li><div class="name"><a href="https://www.tupperware.com.cy/" target="_blank" class="block">Cyprus</a></div><div class="address"><a href="https://www.tupperware.com.cy/" target="_blank" class="block">www.tupperware.com.cy</a></div><div class="clr"></div></li>
                                    <li><div class="name"><a href="https://www.tupperware.cz/" target="_blank" class="block">Czech Republic</a></div><div class="address"><a href="https://www.tupperware.cz/" target="_blank" class="block">www.tupperware.cz</a></div><div class="clr"></div></li>
                                    <li><div class="name"><a href="https://www.tupperware.dk/" target="_blank" class="block">Denmark</a></div><div class="address"><a href="https://www.tupperware.dk/" target="_blank" class="block">www.tupperware.dk</a></div><div class="clr"></div></li>
                                    <li><div class="name"><a href="https://www.tupperware.ee/" target="_blank" class="block">Estonia</a></div><div class="address"><a href="https://www.tupperware.ee/" target="_blank" class="block">www.tupperware.ee</a></div><div class="clr"></div></li>
                                    <li><div class="name"><a href="https://www.tupperware.fi/" target="_blank" class="block">Finland</a></div><div class="address"><a href="https://www.tupperware.fi/" target="_blank" class="block">www.tupperware.fi</a></div><div class="clr"></div></li>
                                    <li><div class="name"><a href="https://www.tupperware.fr/" target="_blank" class="block">France</a></div><div class="address"><a href="https://www.tupperware.fr/" target="_blank" class="block">www.tupperware.fr</a></div><div class="clr"></div></li>
                                    <li><div class="name"><a href="https://www.tupperware.de/" target="_blank" class="block">Germany</a></div><div class="address"><a href="https://www.tupperware.de/" target="_blank" class="block">www.tupperware.de</a></div><div class="clr"></div></li>
                                    <li><div class="name"><a href="https://www.tupperware.gr/" target="_blank" class="block">Greece</a></div><div class="address"><a href="https://www.tupperware.gr/" target="_blank" class="block">www.tupperware.gr</a></div><div class="clr"></div></li>
                                    <li><div class="name"><a href="https://www.tupperware.hu/" target="_blank" class="block">Hungary</a></div><div class="address"><a href="https://www.tupperware.hu/" target="_blank" class="block">www.tupperware.hu</a></div><div class="clr"></div></li>
                                    <li><div class="name"><a href="https://www.tupperware.it/" target="_blank" class="block">Italy</a></div><div class="address"><a href="https://www.tupperware.it/" target="_blank" class="block">www.tupperware.it</a></div><div class="clr"></div></li>
                                    <li><div class="name"><a href="https://www.tupperware.lv/" target="_blank" class="block">Latvia</a></div><div class="address"><a href="https://www.tupperware.lv/" target="_blank" class="block">www.tupperware.lv</a></div><div class="clr"></div></li>
                                    <li><div class="name"><a href="https://www.tupperware.lt/" target="_blank" class="block">Lithuania</a></div><div class="address"><a href="https://www.tupperware.lt/" target="_blank" class="block">www.tupperware.lt</a></div><div class="clr"></div></li>
                                    <li><div class="name"><a href="https://www.tupperware.bg/" target="_blank" class="block">Macedonia</a></div><div class="address"><a href="https://www.tupperware.bg/" target="_black" class="block">www.tupperware.bg</a></div><div class="clr"></div></li>
                                    <li><div class="name"><a href="https://www.tupperware.com.mt/" target="_blank" class="block">Malta</a></div><div class="address"><a href="https://www.tupperware.com.mt/" target="_blank" class="block">www.tupperware.com.mt</a></div><div class="clr"></div></li>
                                    <li><div class="name"><a href="https://www.tupperware.me/" target="_blank" class="block">Montenegro</a></div><div class="address"><a href="https://www.tupperware.me/" target="_blank" class="block">www.tupperware.me</a></div><div class="clr"></div></li>
                                    <li><div class="name"><a href="https://www.tupperware.nl/" target="_blank" class="block">Netherlands</a></div><div class="address"><a href="https://www.tupperware.nl/" target="_blank" class="block">www.tupperware.nl</a></div><div class="clr"></div></li>
                                    <li><div class="name"><a href="https://www.tupperware.no/" target="_blank" class="block">Norway</a></div><div class="address"><a href="https://www.tupperware.no/" target="_blank" class="block">www.tupperware.no</a></div><div class="clr"></div></li>
                                    <li><div class="name"><a href="https://www.tupperware.pl/" target="_blank" class="block">Poland</a></div><div class="address"><a href="https://www.tupperware.pl/" target="_blank" class="block">www.tupperware.pl</a></div><div class="clr"></div></li>
                                    <li><div class="name"><a href="https://www.tupperware.pt/" target="_blank" class="block">Portugal</a></div><div class="address"><a href="https://www.tupperware.pt/" target="_blank" class="block">www.tupperware.pt</a></div><div class="clr"></div></li>
                                    <li><div class="name"><a href="https://www.tupperware.ro/" target="_blank" class="block">Romania</a></div><div class="address"><a href="https://www.tupperware.ro/" target="_blank" class="block">www.tupperware.ro</a></div><div class="clr"></div></li>
                                    <li><div class="name"><a href="https://www.tupperware.ru/" target="_blank" class="block">Russia</a></div><div class="address"><a href="https://www.tupperware.ru/" target="_blank" class="block">www.tupperware.ru</a></div><div class="clr"></div></li>
                                    <li><div class="name"><a href="https://www.tupperware.rs/" target="_blank" class="block">Serbia</a></div><div class="address"><a href="https://www.tupperware.rs/" target="_blank" class="block">www.tupperware.rs</a></div><div class="clr"></div></li>
                                    <li><div class="name"><a href="https://www.tupperware.sk/" target="_blank" class="block">Slovakia</a></div><div class="address"><a href="https://www.tupperware.sk/" target="_blank" class="block">www.tupperware.sk</a></div><div class="clr"></div></li>
                                    <li><div class="name"><a href="https://www.tupperware.si/" target="_blank" class="block">Slovenia</a></div><div class="address"><a href="https://www.tupperware.si/" target="_blank" class="block">www.tupperware.si</a></div><div class="clr"></div></li>
                                    <li><div class="name"><a href="https://www.tupperware.es/" target="_blank" class="block">Spain</a></div><div class="address"><a href="https://www.tupperware.es/" target="_blank" class="block">www.tupperware.es</a></div><div class="clr"></div></li>
                                    <li><div class="name"><a href="https://www.tupperware.se/" target="_blank" class="block">Sweden</a></div><div class="address"><a href="https://www.tupperware.se/" target="_blank" class="block">www.tupperware.se</a></div><div class="clr"></div></li>
                                    <li><div class="name"><a href="https://www.tupperware.ch/" target="_blank" class="block">Switzerland</a></div><div class="address"><a href="https://www.tupperware.ch/" target="_blank" class="block">www.tupperware.ch</a></div><div class="clr"></div></li>
                                    <li><div class="name"><a href="https://www.tupperware.com.tr/" target="_blank" class="block">Turkey</a></div><div class="address"><a href="https://www.tupperware.com.tr/" target="_blank" class="block">www.tupperware.com.tr</a></div><div class="clr"></div></li>
                                    <li><div class="name"><a href="https://www.tupperware.ie/" target="_blank" class="block">UK & Ireland</a></div><div class="address"><a href="https://www.tupperware.ie/" target="_blank" class="block">www.tupperware.ie</a></div><div class="clr"></div></li>
                                    <li><div class="name"><a href="https://www.tupperware.ua/" target="_blank" class="block">Ukraine</a></div><div class="address"><a href="https://www.tupperware.ua/" target="_blank" class="block">www.tupperware.ua</a></div><div class="clr"></div></li>
                                </ul>
                                <ul class="rowAnchor">
                                    <li class="first dottedBottom"><div class="name"><b>Other Brands</b></div><div class="address"><b>Contact</b></div><div class="clr"></div></li>
                                    <li><div class="name"><a href="https://www.nutrimetics.fr/" target="_blank" class="block">Nutrimetics (France)</a></div><div class="address"><a href="https://www.nutrimetics.fr/" target="_blank" class="block">www.nutrimetics.fr</a></div><div class="clr"></div></li>
                                </ul>
                            </div>
                        </div>
                        
                        <div class="content" rel="AF">
                            <h2>Africa</h2>
                            <div class="grayPanel">
                                <ul class="rowAnchor">
                                    <li class="first dottedBottom"><div class="name"><b>Country</b></div><div class="address"><b>Contact</b></div><div class="clr"></div></li>
                                    <li><div class="name"><a href="https://www.tupperware.dz/" target="_blank" class="block">Algeria</a></div><div class="address"><a href="https://www.tupperware.dz/" target="_blank" class="block">www.tupperware.dz</a></div><div class="clr"></div></li>
                                    <li><div class="name"><a href="https://www.tupperware.com.eg/" target="_blank" class="block">Egypt</a></div><div class="address"><a href="https://www.tupperware.com.eg/" target="_blank" class="block">www.tupperware.com.eg</a></div><div class="clr"></div></li>
                                    <li><div class="name"><a href="https://www.tupperwarejordan.com" target="_blank" class="block">Jordan</a></div><div class="address"><a href="https://www.tupperwarejordan.com" target="_blank" class="block">www.tupperwarejordan.com</a></div><div class="clr"></div></li>
                                    <li><div class="name"><a href="https://www.tupperwarelebanon.com/" target="_blank" class="block">Lebanon</a></div><div class="address"><a href="https://www.tupperwarelebanon.com/" target="_blank" class="block">www.tupperwarelebanon.com</a></div><div class="clr"></div></li>
                                    <li><div class="name"><a href="https://www.tupperware.co.ma/" target="_blank" class="block">Morocco</a></div><div class="address"><a href="https://www.tupperware.co.ma/" target="_blank" class="block">www.tupperware.co.ma</a></div><div class="clr"></div></li>
                                    <li><div class="name"><a href="https://www.tupperware.co.za/" target="_blank" class="block">South Africa</a></div><div class="address"><a href="https://www.tupperware.co.za/" target="_blank" class="block">www.tupperware.co.za</a></div><div class="clr"></div></li>
                                    <li><div class="name"><a href="https://www.tupperware.tn/" target="_blank" class="block">Tunisia</a></div><div class="address"><a href="https://www.tupperware.tn/" target="_blank" class="block">www.tupperware.tn</a></div><div class="clr"></div></li>
                                </ul>
                                <ul class="rowAnchor">
                                    <li class="first dottedBottom"><div class="name"><b>Other Brands</b></div><div class="address"><b>Contact</b></div><div class="clr"></div></li>
                                    <li><div class="name"><a href="http://www.avroyshlain.com" target="_blank" class="block">Avroy Shlain (South Africa)</a></div><div class="address"><a href="http://www.avroyshlain.com" target="_blank" class="block">www.avroyshlain.com</a></div><div class="clr"></div></li>
                                </ul>
                            </div>
                            
                        </div>
                        
                        <div class="content" rel="AP">
                            <h2>Asia &amp; Pacific</h2>
                            <div class="grayPanel">
                                <ul class="rowAnchor">
                                    <li class="first dottedBottom"><div class="name"><b>Country</b></div><div class="address"><b>Contact</b></div><div class="clr"></div></li>
                                    <li><div class="name"><a href="https://www.tupperware.com.au/" target="_blank" class="block">Australia </a></div><div class="address"><a href="https://www.tupperware.com.au/" target="_blank" class="block">www.tupperware.com.au</a></div><div class="clr"></div></li>
                                    <li><div class="name"><a href="https://www.tupperware.com.cn/" target="_blank" class="block">China</a></div><div class="address"><a href="https://www.tupperware.com.cn/" target="_blank" class="block">www.tupperware.com.cn</a></div><div class="clr"></div></li>
                                    <li><div class="name"><a href="https://www.tupperwareindia.com/" target="_blank" class="block">India</a></div><div class="address"><a href="https://www.tupperwareindia.com/" target="_blank" class="block">www.tupperwareindia.com</a></div><div class="clr"></div></li>
                                    <li><div class="name"><a href="https://www.tupperware.co.id/" target="_blank" class="block">Indonesia</a></div><div class="address"><a href="https://www.tupperware.co.id/" target="_blank" class="block">www.tupperware.co.id</a></div><div class="clr"></div></li>
                                    <li><div class="name"><a href="https://www.tupperware.co.il/" target="_blank" class="block">Israel</a></div><div class="address"><a href="https://www.tupperware.co.il/" target="_blank" class="block">www.tupperware.co.il</a></div><div class="clr"></div></li>
                                    <li><div class="name"><a href="https://www.tupperware.co.jp/" target="_blank" class="block">Japan</a></div><div class="address"><a href="https://www.tupperware.co.jp/" target="_blank" class="block">www.tupperware.co.jp</a></div><div class="clr"></div></li>
                                    <li><div class="name"><a href="http://www.tupperware.co.kr/" target="_blank" class="block">Korea</a></div><div class="address"><a href="http://www.tupperware.co.kr/" target="_blank" class="block">www.tupperware.co.kr</a></div><div class="clr"></div></li>
                                    <li><div class="name"><a href="http://www.tupperwarebrands.com.my/" target="_blank" class="block">Malaysia</a></div><div class="address"><a href="http://www.tupperwarebrands.com.my/" target="_blank" class="block">www.tupperwarebrands.com.my</a></div><div class="clr"></div></li>
                                    <li><div class="name"><a href="https://www.tupperware.co.nz/" target="_blank" class="block">New Zealand</a></div><div class="address"><a href="https://www.tupperware.co.nz/" target="_blank" class="block">www.tupperware.co.nz</a></div><div class="clr"></div></li>
                                    <li><div class="name"><a href="https://www.tupperwarebrands.ph/" target="_blank" class="block">Philippines</a></div><div class="address"><a href="https://www.tupperwarebrands.ph/" target="_blank" class="block">www.tupperwarebrands.ph</a></div><div class="clr"></div></li>
                                    <li><div class="name"><a href="https://www.tupperware.ru/" target="_blank" class="block">Russia</a></div><div class="address"><a href="https://www.tupperware.ru/" target="_blank" class="block">www.tupperware.ru</a></div><div class="clr"></div></li>
                                    <li><div class="name"><a href="https://tupperwarebrands.sg" target="_blank" class="block">Singapore</a></div><div class="address"><a href="https://tupperwarebrands.sg" target="_blank" class="block">tupperwarebrands.sg</a></div><div class="clr"></div></li>
                                    <li><div class="name"><a href="javascript:void(0)" class="block">Taiwan</a></div><div class="address"><a href="javascript:void(0)" class="block">886-332-73111</a></div><div class="clr"></div></li>
                                    <li><div class="name"><a href="javascript:void(0)" class="block">Thailand</a></div><div class="address"><a href="javascript:void(0)" class="block">66-2-750-2190</a></div><div class="clr"></div></li>
                                    <li><div class="name"><a href="https://www.tupperware.com.vn/" target="_blank" class="block">Vietnam</a></div><div class="address"><a href="https://www.tupperware.com.vn/" target="_blank" class="block">www.tupperware.com.vn</a></div><div class="clr"></div></li>
                                </ul>
                                <ul class="rowAnchor">
                                    <li class="first dottedBottom"><div class="name"><b>Other Brands</b></div><div class="address"><b>Contact</b></div><div class="clr"></div></li>
                                    <li><div class="name"><a href="http://www.naturcare.co.jp/" target="_blank" class="block">NaturCare (Japan)</a></div><div class="address"><a href="http://www.naturcare.co.jp" target="_blank" class="block">www.naturcare.co.jp</a></div><div class="clr"></div></li>
                                    <li><div class="name"><a href="http://www.nutrimetics.com.au/" target="_blank" class="block">Nutrimetics (Australia)</a></div><div class="address"><a href="http://www.nutrimetics.com.au/" target="_blank" class="block">www.nutrimetics.com.au</a></div><div class="clr"></div></li>
                                    <li><div class="name"><a href="http://www.nutrimetics.co.nz/" target="_blank" class="block">Nutrimetics (New Zealand)</a></div><div class="address"><a href="http://www.nutrimetics.co.nz/" target="_blank" class="block">www.nutrimetics.co.nz</a></div><div class="clr"></div></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
              
                
                <script type="text/javascript">
                    afterInit.push(function(){
                        internationalListing.init({
                            map: '/wp-content/uploads/2020/09/world-map.png',
                            layers: [
                                {id: 'NA', pos:[90,170], text: "North America"},
                                {id: 'SA', pos:[160,390], text: "South America"},
                                {id: 'EU', pos:[416,155], text: "Europe"},
                                {id: 'AF', pos:[410,300], text: "Africa"},
                                {id: 'AP', pos:[650,262], text: "Asia &amp; Pacific"}
                            ]
                        });
                    });
                </script>
                
                                                    
                
            </div>
        </div>
        

                            
    </section>
</div>

</div>
<?php get_footer(); ?>