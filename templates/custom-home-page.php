<?php

/*
 * Template Name: Home Page
 * description: >-
 */
 
$detect = new Mobile_Detect;

?>

<?php get_header(); ?>
<div id="content">
    <section class="row">
        
        <div class="flexslider">
            <ul class="slides">
                <?php

                    // Check rows exists.
                    if( have_rows('slider_wrapper') ):

                        // Loop through rows.
                        while( have_rows('slider_wrapper') ) : the_row();
                            
                            if( have_rows('slide') ):
                                while( have_rows('slide') ): the_row();

                                    $file = get_sub_field('image_desktop');
                                    $imageMobile = get_sub_field('image_mobile');
                                    $link = get_sub_field('link');
                                    $linkText = get_sub_field('link_text');
                                    $tagLine = get_sub_field('tag_line');
                                ?>

                                <li>
                                    
                                    <?php if( $file['type'] == 'image' ) { ?>

                                        <?php if ( $detect->isMobile() ) { ?>
                                        
                                            <img src="<?php echo esc_url( $imageMobile['url'] ); ?>" />

                                        <?php }else{ ?>
                                            
                                            <img src="<?php echo esc_url( $file['url'] ); ?>" />

                                        <?php } ?>

                                    <?php }elseif ($file['type'] == 'video') { ?>
                                        
                                        <video style="width:100%;" src="<?php echo esc_url( $file['url'] ); ?>" ></video>

                                    <?php } ?>

                                    <?php if(!empty($tagLine)){ ?>
                                        <h2 id="sliderh2"><?php echo $tagLine; ?></h2>
                                    <?php } ?>

                                    <a href="<?php echo $link; ?>" onclick="ga('send', 'event', 'Home Facelift', 'Button A', 'Learn More');" data-initialized="true">
                                        <button class="button"><?php echo $linkText; ?></button>
                                    </a>

                                </li>

                                <?php endwhile;
                            endif;

                        // End loop.
                        endwhile;

                    // No value.
                    else :
                        // Do something...
                    endif;
                ?>
            </ul>
        </div>
        <div class="twoColStage">
            
            <?php
            if( have_rows('column_1_group') ):
                while( have_rows('column_1_group') ): the_row(); 

                    $image = get_sub_field('image');
                    $link = get_sub_field('link');
                    $linktext = get_sub_field('link_text');
                    ?>

                    <div class="col stage" data-type="picture">
                        <div class="placeholder" data-image-portrait="<?php echo esc_url( $image['url'] ); ?>" data-image-landscape="<?php echo esc_url( $image['url'] ); ?>">
                            <a href="<?php echo $link; ?>" onclick="ga('send', 'event', 'Home Facelift', 'Button B', 'See Our Specials');">
                                <div class="aligner">
                                    <div class="cell">
                                        <div class="row">
                                            <span class="button"><?php echo $linktext; ?></span>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>

                <?php endwhile; ?>
            <?php endif; ?>
            

            <?php
            if( have_rows('column_2_group') ):
                while( have_rows('column_2_group') ): the_row(); 

                    $image = get_sub_field('image');
                    $link = get_sub_field('link');
                    $linktext = get_sub_field('link_text');
                    ?>

                    <div class="col stage" data-type="picture">
                        <div class="placeholder" data-image-portrait="<?php echo esc_url( $image['url'] ); ?>" data-image-landscape="<?php echo esc_url( $image['url'] ); ?>" >
                            <a href="<?php echo $link; ?>" onclick="ga('send', 'event', 'Home Facelift', 'Button C', 'Start A Business');">
                                <div class="aligner">
                                    <div class="cell">
                                        <div class="row">
                                            <span class="button"><?php echo $linktext; ?></span>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>
                    </div>

                <?php endwhile; ?>
            <?php endif; ?>
            
            <div class="clr"></div>
        </div>
    </section>
</div>
<?php get_footer(); ?>