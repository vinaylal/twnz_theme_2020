<?php
/*
 * Template Name: Product Category Page
 * description: >-
 */
?>
<?php get_header(); ?>
<div id="content">
    <section class="row">
        <div id="leftColumn" class="col">
            <div class="wrapper">
                <div id="leftNavigation">
                    <?php get_template_part( 'template-parts/categories-menu' ); ?>
                </div>
            </div>
        </div>
    
        <div id="middleColumn" class="col">
            <div class="wrapper">
                <?php
                    if ( have_posts() ) {
                        while ( have_posts() ) {
                            the_title( '<h1 class="entry-title">', '</h1>' );
                            the_post();
                            the_content();
                        } // end while
                    } // end if
                ?>
            </div>
        </div>           
    </section>
</div>
<?php get_footer(); ?>