

<div id="leftColumn" class="col">
    <div class="wrapper">
        <div id="leftNavigation">
            
            <?php get_template_part( 'template-parts/categories-menu' ); ?>
            
        </div>
        
    </div>
</div>

<div id="middleColumn" class="col">
    <div class="wrapper">
        <div class="contentRow ">
            <div class="clr"></div>
        </div>


<div class="contentRow detailInfo" data-promise-product-item-id="20668" data-promise-variant-id="25"><a class="back" href="javascrip:void(0)" rel="backbutton"><span>Back to results</span></a><h1>Grow With Me Straw Tumbler</h1>
<div class="cols">
<div class="col-left">
<div class="container">
<div id="slideshowModule" class="displayWindow p">
<div class="border">
<div class="wrapper">
<ul class="holder">
<li data-start-class="start-zoomOut" data-end-class="end-zoomIn" class=""><a href="/service/appng/tupperware-products/webservice/images/272131_1200x845.jpg" rel="prettyPhoto[product]" class="withoutImageZoom"><img class="default" src="/service/appng/tupperware-products/webservice/images/272131_666x468.jpg" alt="Tupperware Grow With Me Straw Tumbler "></img></a></li>
</ul>
</div>
</div>
</div><script type="text/javascript">
/**/
var displayWindowsObj;
displayWindowsData.push({
selector: '#slideshowModule.displayWindow ul.holder',
options: { 
controlSelector: '.displayWindow .border', 
paginationEnable: true,
controlEnable: true,
hideControl: false,
diashowInterval: 0,
animation: 'fade',
loopEnabled: true,
afterInit: function(self){
    displayWindowsObj = self;
},
beforeChangeActiveItem: function() {
	if (typeof window['youTubeVideoPlayer-000'] != 'undefined') window['youTubeVideoPlayer-000'].pauseVideo();
}
}
});
/**/
</script></div>
<div class="buttonsPanel">
<ul class="buttonsLine">

            <li class="right placeholder">
                <div class="slider hover-color-blue">
                    
                    <ul rel="share" data-mobile-title="Share">
                        <li><a href="javascript:void(0)" class="icon share-alt icon-only">Share</a></li>
                        
<li><a href="javascript:void(0)" data-network="facebook" class="st-custom-button st_facebook_custom icon facebook icon-only" title="Facebook">Facebook</a></li>

<li><a href="javascript:void(0)" data-network="googleplus" class="st-custom-button st_googleplus_custom icon googleplus icon-only" title="Google+">Google+</a></li>

<li><a href="javascript:void(0)" data-network="pinterest" class="st-custom-button st_pinterest_custom icon pinterest icon-only" title="Pinterest">Pinterest</a></li>

                        <li><a href="javascript:void(0)" data-network="sharethis" class="st-custom-button st_sharethis_custom icon share icon-only" title="ShareThis">ShareThis</a></li> 
                    </ul>
                    <div class="clr"></div>
                </div>
            </li>


<li class="right"><a class="icon icon-only heart tooltip" href="javascript:void(0)" onclick="wishlist.addProduct('20668');" data-add-wishlist-product-id="20668" rel="Would you like to add this to your Wishlist?"><span>Add to Wishlist</span></a></li>
</ul>
<div class="clr"></div>
</div>
<div class="infoPanel"><span class="price"><span class="regular">NZD $ 30.00</span></span><span class="price"><span class="regular">AUD $ 26.00</span></span><div class="clr"></div>
</div>
<div class="tabs" data-media="not-for-mobile">
<div class="tabButtonsPanel"></div>
<div class="tabItem" rel="product-details">
<div class="tabButtons">
<ul>
<li class="selected"><a href="javascript:void(0)"><span>Product details</span></a></li>
</ul>
<p class="clr"> </p>
</div>
<div>
<h2 class="desktop-only">Grow With Me Straw Tumbler</h2>
<div class="text short"><p>The Grow With Me Straw Tumbler is an ideal on-the-go beverage solution for older babies and toddlers. The slider on the Cap is easy for little hands to use, and houses a flexible straw that is soft on baby&rsquo;s mouth. When closed, the Tumbler is liquid tight and can be safely transported in the handbag, nappy bag or backpack. When open, the flow of liquid is restricted thanks to the one-way valve, thereby reducing the amount of liquid that can accidentally spill from the container.<br /><br />Suitable for water, milk and non-carbonated beverages, both in the home or on the go. <br />Suitable for children from 6 months.<br /><br /><strong>Dimensions:</strong><br />Tumbler with Cap closed, 350ml D 7.4 H 15.6<br />Straw Cleaning Brush L 15 <br /><br /><strong>Care:</strong></p>
<ul>
<li>Lifetime Guarantee</li>
<li>Dishwasher Safe. Hand wash recommended for straw and tumbler</li>
<li>Not suitable for the Microwave, Freezer or Oven</li>
</ul></div>
</div>
</div>
</div>
</div>
<div class="col-right" id="rightColumn"></div><script type="text/javascript">
bufferScarabQueue.push(['view', unescapeHtml('20668')]);
</script><div class="clr"></div>
</div>
</div>


<script type="text/x-tmpl" id="right-column-of-details-page">

					<div class="contentRow">
						<div class="banner connect-banner">
						    <a href="https://www.tupperware.com.au/connect" ><h3>Want to buy?</h3></a>
							<a href="https://www.tupperware.com.au/connect" ><img src="/assets/images/connect-logo.png" alt="Find a consultant" /></a>
							<a href="https://www.tupperware.com.au/connect"  class="button bold"><span>Find a consultant</span></a>
						</div>
					</div>
					&gt;script type="text/javascript">
					    
					&gt;/script>												<div class="contentRow">
<div class="tabs" id="relatedItems">
<div class="tabItem" rel="related-related">
<h4>You may also like</h4>
<div>
<div class="text">
<ul class="teasersList">
<li><a href="/products/20665/InfantFeedingSet"><img src="/service/appng/tupperware-products/webservice/images/271651_60x60.jpg" alt="Infant Feeding Set"></img><div><span class="title">Infant Feeding Set</span><span class="text"><span class="price"><span class="regular">NZD $ 75.00</span></span></span><span class="text"><span class="price"><span class="regular">AUD $ 65.00</span></span></span></div>
<div class="clr"></div></a></li>
<li><a href="/products/20674/LetsEatSet"><img src="/service/appng/tupperware-products/webservice/images/271660_60x60.jpg" alt="Let's Eat Set"></img><div><span class="title">Let's Eat Set</span><span class="text"><span class="price"><span class="regular">NZD $ 56.00</span></span></span><span class="text"><span class="price"><span class="regular">AUD $ 49.00</span></span></span></div>
<div class="clr"></div></a></li>
<li><a href="/products/13721/ShapeO"><img src="/service/appng/tupperware-products/webservice/images/178115_60x60.jpg" alt="Shape-O"></img><div><span class="title">Shape-O</span><span class="text"><span class="price"><span class="regular">NZD $ 63.00</span></span></span><span class="text"><span class="price"><span class="regular">AUD $ 55.00</span></span></span></div>
<div class="clr"></div></a></li>
<li><a href="/products/20677/SolidStartSet"><img src="/service/appng/tupperware-products/webservice/images/271663_60x60.jpg" alt="Solid Start Set"></img><div><span class="title">Solid Start Set</span><span class="text"><span class="price"><span class="regular">NZD $ 18.00</span></span></span><span class="text"><span class="price"><span class="regular">AUD $ 16.00</span></span></span></div>
<div class="clr"></div></a></li>
<li><a href="/products/20671/ToddlerFeedingSet"><img src="/service/appng/tupperware-products/webservice/images/271657_60x60.jpg" alt="Toddler Feeding Set"></img><div><span class="title">Toddler Feeding Set</span><span class="text"><span class="price"><span class="regular">NZD $ 30.00</span></span></span><span class="text"><span class="price"><span class="regular">AUD $ 26.00</span></span></span></div>
<div class="clr"></div></a></li>
</ul>
</div>
</div>
</div>
</div>
</div>

<div class="contentRow desktop-only">
<div id="menuCoverTabFilter" class="tabs">
<div class="tabButtonsPanel"></div>
<div class="tabItem " rel="product-categories"><a class="anchor-to-delete" name="product-categories"></a><div class="tabButtons">
<ul>
<li class="selected"><a href="javascript:void(0)"><span>Categories</span></a></li>
</ul>
<p class="clr"> </p>
</div>
<div class="part2">
<ul>

<li>
<div class="wrapper"><a href="/shop/all-products">All Products</a></div>
</li>

<li>
<div class="wrapper"><a href="/shop/all-products?category=128#category">Baking</a></div>
</li>

<li>
<div class="wrapper"><a href="/shop/all-products?category=173#category">Cookware</a></div>
</li>

<li class="selected">
<div class="wrapper">
<div class="bg"><a href="/shop/all-products?category=810#category">Kids &amp; Babies</a></div>
&gt;script>
bufferScarabQueue.push(['category', unescapeHtml('Kids &amp; Babies')]);
&gt;/script></div>
</li>

<li>
<div class="wrapper"><a href="/shop/all-products?category=26#category">Knives</a></div>
</li>

<li>
<div class="wrapper"><a href="/shop/all-products?category=831#category">Lunch boxes</a></div>
</li>

<li>
<div class="wrapper"><a href="/shop/all-products?category=707#category">On-The-Go</a></div>
</li>

<li>
<div class="wrapper"><a href="/shop/all-products?category=158#category">Serving</a></div>
</li>

<li>
<div class="wrapper"><a href="/shop/all-products?category=38#category">Storage</a></div>
</li>

<li>
<div class="wrapper"><a href="/shop/all-products?category=41#category">Tools &amp; Gadgets</a></div>
</li>

<li>
<div class="wrapper"><a href="/shop/all-products?category=1261#category">Water bottles</a></div>
</li>

<li>
<div class="wrapper"><a href="/shop/all-products?category=1409#category">Sustainability</a></div>
</li>

<li>
<div class="wrapper"><a href="/shop/all-products?category=1412#category">Cooking</a></div>
</li>

<li>
<div class="wrapper"><a href="/shop/all-products?category=32#category">Microwave Cooking</a></div>
</li>

<li>
<div class="wrapper"><a href="/shop/all-products?category=1415#category">Dual Oven Cooking</a></div>
</li>

<li>
<div class="wrapper"><a href="/shop/all-products?category=1418#category">Cooking Prep</a></div>
</li>

<li>
<div class="wrapper"><a href="/shop/all-products?category=899#category">Reheating</a></div>
</li>
</ul>
</div>
</div>
<div class="tabItem " rel="product-collections"><a class="anchor-to-delete" name="product-collections"></a><div class="tabButtons">
<ul>
<li class="selected"><a href="javascript:void(0)"><span>Collections</span></a></li>
</ul>
<p class="clr"> </p>
</div>
<div class="part2">
<ul>

<li>
<div class="wrapper"><a href="/shop/all-products">All Products</a></div>
</li>

<li class="selected">
<div class="wrapper">
<div class="bg"><a href="/shop/all-products?collection=1234#collection">Babies and Toddlers</a></div>
</div>
</li>

<li>
<div class="wrapper"><a href="/shop/all-products?collection=194#collection">Chef Series</a></div>
</li>

<li>
<div class="wrapper"><a href="/shop/all-products?collection=1231#collection">Clarity Canisters</a></div>
</li>

<li>
<div class="wrapper"><a href="/shop/all-products?collection=1225#collection">Clear Bowls</a></div>
</li>

<li>
<div class="wrapper"><a href="/shop/all-products?collection=188#collection">Clear Mates</a></div>
</li>

<li>
<div class="wrapper"><a href="/shop/all-products?collection=637#collection">Cookware</a></div>
</li>

<li>
<div class="wrapper"><a href="/shop/all-products?collection=1188#collection">Drink bottles</a></div>
</li>

<li>
<div class="wrapper"><a href="/shop/all-products?collection=1228#collection">Entertain</a></div>
</li>

<li>
<div class="wrapper"><a href="/shop/all-products?collection=248#collection">EZ System</a></div>
</li>

<li class="selected">
<div class="wrapper">
<div class="bg"><a href="/shop/all-products?collection=1237#collection">Feeding Set</a></div>
</div>
</li>

<li>
<div class="wrapper"><a href="/shop/all-products?collection=269#collection">Freezer</a></div>
</li>

<li>
<div class="wrapper"><a href="/shop/all-products?collection=1058#collection">FreezerKeeper</a></div>
</li>

<li>
<div class="wrapper"><a href="/shop/all-products?collection=257#collection">Fridge</a></div>
</li>

<li>
<div class="wrapper"><a href="/shop/all-products?collection=1222#collection">Grow With Me</a></div>
</li>

<li>
<div class="wrapper"><a href="/shop/all-products?collection=684#collection">Heat ‘N Eats</a></div>
</li>

<li>
<div class="wrapper"><a href="/shop/all-products?collection=41#collection">Kids</a></div>
</li>

<li>
<div class="wrapper"><a href="/shop/all-products?collection=149#collection">Kitchen Tools</a></div>
</li>

<li>
<div class="wrapper"><a href="/shop/all-products?collection=197#collection">Knives</a></div>
</li>

<li>
<div class="wrapper"><a href="/shop/all-products?collection=1209#collection">KP Tools</a></div>
</li>

<li>
<div class="wrapper"><a href="/shop/all-products?collection=1046#collection">Lunch</a></div>
</li>

<li>
<div class="wrapper"><a href="/shop/all-products?collection=86#collection">Microwave Cooking</a></div>
</li>

<li>
<div class="wrapper"><a href="/shop/all-products?collection=607#collection">Modular Mates</a></div>
</li>

<li class="selected">
<div class="wrapper">
<div class="bg"><a href="/shop/all-products?collection=634#collection">New</a></div>
</div>
</li>

<li>
<div class="wrapper"><a href="/shop/all-products?collection=1191#collection">Pantry</a></div>
</li>

<li>
<div class="wrapper"><a href="/shop/all-products?collection=1194#collection">Picnic</a></div>
</li>

<li>
<div class="wrapper"><a href="/shop/all-products?collection=1061#collection">Silicone</a></div>
</li>

<li>
<div class="wrapper"><a href="/shop/all-products?collection=152#collection">Time Savers</a></div>
</li>

<li>
<div class="wrapper"><a href="/shop/all-products?collection=1091#collection">TupperChef</a></div>
</li>

<li>
<div class="wrapper"><a href="/shop/all-products?collection=1112#collection">UltraPro</a></div>
</li>

<li>
<div class="wrapper"><a href="/shop/all-products?collection=1212#collection">Universal Series</a></div>
</li>

<li>
<div class="wrapper"><a href="/shop/all-products?collection=131#collection">VentSmart</a></div>
</li>
</ul>
</div>
</div>
</div>
<div class="clr"> </div>
</div>


</script>


<script type="text/x-tmpl" id="item-comments">
{% if (o.results.data.length > 0) { %}
<div class="info-panel with-borders">

{%
var rFloor = Math.floor(o.average);
var def = (o.average - rFloor)*10;
var rClass = "s";
if (def < 3) {
rClass = rClass+rFloor;
} else if ((def >=3) && (def <=7)) {
rClass = rClass+rFloor+'h';
} else {
rClass = rClass+(rFloor+1);
}
%}
<ul class="items-line mobile-only with-border">
<li class="default"><span class="rating-stars {%=rClass%}"></span> {%=o.average%} / 5 stars</li>
</ul>
<ul class="items-line">
<li class="default">{%=o.overall%} reviews</li>
<li class="separator"></li>
<li class="default desktop-only"><span class="rating-stars {%=rClass%}"></span> {%=o.average%} / 5 stars</li>

{% if (typeof o.results.sortBy != "undefined") { %}
{%
var activeSortOption = 0;
for (var i=0; i<o.results.sortBy.data.length;i++) {
activeSortOption = (o.results.sortBy.data[i].active) ? i : activeSortOption; 
}
%}
<li class="separator desktop-only"></li>
<li>
<ul class="controlPanel">
<li class="withDropdownPanel">
<a href="javascript:void(0)">{%=o.results.sortBy.config.label%}: <em>{%=o.results.sortBy.data[activeSortOption].label%}</em></a>
<div class="dropdownPanel" style="display: none;">
<div class="wrapper">
	<div class="col">
		<ul>
			{% for (var i=0;i<o.results.sortBy.data.length;i++) { %}
			<li><label class="radio {% if (o.results.sortBy.data[i].active) { %}active{% } %}"><input type="radio" name="{%=o.results.sortBy.config.name%}" value="{%=o.results.sortBy.data[i].value%}" {% if (o.results.sortBy.data[i].active) { %}checked{% } %} onchange="comments.setSort('{%=o.results.sortBy.config.name%}')" />{%=o.results.sortBy.data[i].label%}</label></li>
			{% } %}
		</ul>
	</div>
	<div class="clr"></div>
</div>
</div>
</li>

</ul>
</li>
{% } %}

</ul>
</div>
{%
var visibleItemsLimit = (o.results.data.length > 3) ? 3 : o.results.data.length;
%}
{% for (var i=0; i<visibleItemsLimit; i++) { %}
<div class="comment-item">
{% if (typeof o.results.data[i].title != 'undefined' ) { %}<h3>{%=o.results.data[i].title%}</h3>{% } %}
<h5>{%=o.results.data[i].author%}</h5>
<ul class="rating"><li><span class="rating-stars small s{%=o.results.data[i].rating%}"></span></li><li><span>{%=o.results.data[i].outputDate%}</span></li><li class="clr"></li></ul>
<p class="comment-text">{%  print(o.results.data[i].text.replace(/\r\n/g, '<br/>'), true); %}</p>
</div>
{% } %}

{% if (o.results.data.length > 3 ) { %}

<div style="display: none;" rel="more-comments">

{% for (var i=3; i<o.results.data.length; i++) { %}
<div class="comment-item">
{% if (typeof o.results.data[i].title != 'undefined' ) { %}<h3>{%=o.results.data[i].title%}</h3>{% } %}
<h5>{%=o.results.data[i].author%}</h5>
<ul class="rating"><li><span class="rating-stars small s{%=o.results.data[i].rating%}"></span></li><li><span>{%=o.results.data[i].outputDate%}</span></li><li class="clr"></li></ul>
<p class="comment-text">{%  print(o.results.data[i].text.replace(/\r\n/g, '<br/>'), true); %}</p>
</div>
{% } %}

</div>

<div class="comment-item text-align-center">
<a href="javascript:void(0)" class="toggle-content" onclick="app.toggleContent(this)" data-rel="more-comments">
<span class="more-content">Show {%=(o.results.data.length-3)%} more</span>
<span class="less-content" style="display: none;">Hide comments</span>
</a>
</div>																	

{% } %}
{% } %}	
</script>     

                         <script type="text/javascript">
                             beforeInit.push(function(){
                                  var rightColumnMarkup = $('#right-column-of-details-page').text().replace(/&gt;script/g, '<script').replace(/&gt;\/script/g,'</script');
                                  $('#rightColumn').append(rightColumnMarkup);
                             });
                         </script>
                         
                        <script type="text/javascript">
                            afterInit.push(function(){
                                app.initBackButtons();
                            });
                        </script>

                        <script type="text/javascript">
                             beforeInit.push(function(){
                                  commentForm.init({
                                     formURL: '/en/product-comment-form',
                                     itemId: '20668'
                                 });
                             });
                         </script>
<!-- -->
<div id="connect-panel" class="connect-panel">
<div class="content">
<div rel="geo-location-detected">
	<h2>Hi there! These Consultants are Near You.</h2>
	<p>Do you know that <strong>Tupperware consultants live right near you!</strong> Don’t hesitate to get in touch with us.</p>
	<div class="ds-list">
		<div class="jcarousel-wrapper">
			<div class="jcarousel">
				<ul>
				</ul>
			</div>
			<a href="#" class="jcarousel-control-prev">&lsaquo;</a>
			<a href="#" class="jcarousel-control-next">&rsaquo;</a>
		</div>								
	</div>
	<ul class="controlPanel">
		<li>
			<a href="https://www.tupperware.com.au/connect" target="_blank" data-rel="connect-link"  class="button bold"><span>See More Consultants Near You</span></a>												
		</li>
	</ul>
</div>
<div rel="geo-location-undefined">
	<h2>Hi There!</h2>
	<p>If you dont already have a <strong>Tupperware Consultant</strong>, you can <strong>find one near you!</strong></p>

	<form action="https://www.tupperware.com.au/connect" data-rel="connect-link" method="GET" target="_blank">
		<div class="row">
			<input type="text" name="location" placeholder="City or Postal Code" value="">
			<input type="submit" name="submit" value="Connect">
		</div>
	</form>
	
</div>
</div>
</div>
<script type="text/x-tmpl" id="connect-list-item">
<li>
<div class="item">
	<div class="photo">
		<a href="{%=o.url%}" class="contact" target="_blank" onclick=""><img src="{%=o.picture%}" alt="{%=o.name%}"></a>
	</div>
	<div class="details">
		<h4>{%=o.name%}</h4>
		<span class="location"><i>{%=o.location%}</i></span>
		<a href="{%=o.url%}" class="more" target="_blank" onclick="">Connect</a>
	</div>
</div>			
</li>
</script>
<script type="text/javascript">
afterInit.push(function(){

var productName = $('h1').text().trim();
productName = (productName) ? productName.replace(/\ /g, '-') : productName;
productName = (productName) ? productName.replace(/\ /g, '-').replace(/,/g,'_').replace(/\./g,'_').toLowerCase() : productName;

if (typeof(dealerInYourArea) != 'undefined') dealerInYourArea.init();
});
</script>
        
                                            
        
    </div>
</div>


                    
</section>
</div>