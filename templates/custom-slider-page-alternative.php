<?php

/*
 * Template Name: Custom Slider Page ALternative
 * description: >-
 */
 
 $detect = new Mobile_Detect;
?>

<?php get_header(); ?>
<div id="content">
    <section class="row">
        
        <div class="flexslider">
            <ul class="slides">
                <?php

                    // Check rows exists.
                    if( have_rows('slider_wrapper') ):

                        // Loop through rows.
                        while( have_rows('slider_wrapper') ) : the_row();
                            
                            if( have_rows('slide') ):
                                while( have_rows('slide') ): the_row();

                                    $file = get_sub_field('image_desktop');
                                    $imageMobile = get_sub_field('image_mobile');
                                    $link = get_sub_field('link');
                                    $linkText = get_sub_field('link_text');
                                    $tagLine = get_sub_field('tag_line');
                                ?>

                                <li>
                                    
                                    <?php if( $file['type'] == 'image' ) { ?>

                                        <?phph if ( $detect->isMobile() ) { ?>
                                        
                                            <img src="<?php echo esc_url( $imageMobile['url'] ); ?>" />

                                        <?php }else{ ?>
                                            
                                            <img src="<?php echo esc_url( $file['url'] ); ?>" />

                                        <?php } ?>

                                    <?php }elseif ($file['type'] == 'video') { ?>
                                        
                                        <video src="<?php echo esc_url( $file['url'] ); ?>" ></video>

                                    <?php } ?>

                                    <!-- <?php echo esc_url( $imageMobile['url'] ); ?> -->

                                    <?php if(!empty($tagLine)){ ?>
                                        <h3><?php echo $tagLine; ?></h3>
                                    <?php } ?>

                                    <a href="<?php echo $link; ?>" onclick="ga('send', 'event', 'Home Facelift', 'Button A', 'Learn More');" data-initialized="true">
                                        <button class="button"><?php echo $linkText; ?></button>
                                    </a>

                                </li>

                                <?php endwhile;
                            endif;

                        // End loop.
                        endwhile;

                    // No value.
                    else :
                        // Do something...
                    endif;
                ?>
            </ul>
        </div>
        
        <div id="middleColumn" class="col">
            <div class="wrapper">

                <?php 
                    if ( have_posts() ) {
                        while ( have_posts() ) {
                            the_title( '<h1 class="entry-title">', '</h1>' );
                            the_post();
                            the_content();
                        } // end while
                    } // end if
                ?>                                
                
            </div>
        </div>
        
    </section>
</div>
<?php get_footer(); ?>