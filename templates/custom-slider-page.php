<?php

/*
 * Template Name: Slider Page
 * description: >-
 */

?>

<?php get_header(); ?>
                    
<div id="content">
    <section class="row">
    

       <!-- -->

                    <div class="contentRow">

                    
                    <div class="description bg">
						<h1>Let your tastebuds travel</h1>
						<div class="intro"><p>Bring a mix of flavours, spices and cuisines to your table this month with our exciting new offers for the whole family to enjoy. <strong>Hurry, only while stocks last.</strong></p>

<p><strong><a href="https://www.tupperware.com.au/connect/">Find a consultant to buy now or book a party.</a></strong></p></div>                                                           
                    </div>
                    
                    
                    <iframe class="responsive" id="iFrame-1521" src="https://tupperware.ipapercms.dk/Tupperware/Australia/monthly-specials/?DisableScrollZoom=true" name="resIFrame1521" data-extend-source="catalogPage" scrolling="no" frameborder="0" style="background-color:#f7f7f7;"></iframe>
                    
                    <script>
                        var iFrameType = "normal";
                        if (iFrameType=="normal") {
                            afterInit.push(function(){
                                var setIFrameHeight = function(jIFrame){
                                    var height = jIFrame.width()*$(window).height()/$(window).width();
                                    height = ($('html').hasClass('home') && !isMobile.any()) ? (height-$('header').outerHeight()) : height;
                                    jIFrame.css('height', height+'px');
                                    //if ($('html').hasClass('home')) jIFrame.parent().css('margin-top', ($('header').outerHeight()+'px'));
                                }
                                setIFrameHeight($('iframe.responsive'));
                                $(window).on('windowSizeChanged', function(){
                                    setIFrameHeight($('iframe.responsive'));
                                })
                            });                                                 
                        } else {
                            messageReceiverData.push({senderDomain: 'https://tupperware.ipapercms.dk'});    
                        }
                    </script>
                    
                </div>

                            
    </section>
</div>

<?php get_footer(); ?>